/********************************************************************
*
* resource.h - Resource ID list for LAMPWords.
* Copyright 2001-2002 Paul Sidorsky
*
* Current Version:  X  (April 1st, 2007)
*   - See HISTORY file for changes and additions.
*
* Requires PRC-Tools 2.0 or above (or Palm OS Developer Suite).
* Requires Palm OS SDK 4.0 or above.
*
* Terms:
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*********************************************************************/

/* Menu IDs */
#define MainMenu                   10100

#define HelpMenuInst               10105
#define HelpMenuDictInfo           10109
#define HelpMenuAbout			   10110

#define OptMenuPrefs               10150
#define OptMenuClear               10160
#define OptMenuBeamDict            10170

/* Forms */
#define LWForm                      1100
#define PrefsForm                   1110
#define WJForm                      1120
#define FitForm                     1130

/* Alert Boxes */
#define AboutAlert                  1500
#define DictInfoAlert               1505
#define DBErrorAlert                1510
#define DBOldAlert                  1515
#define OutOfMemAlert               1520
#define ListFullAlert               1530
#define ListDBAlert                 1540
#define RulingAlert                 1550

/* Groups */
#define ModeGroup                      1

/* Controls - Main Form */
#define CatTrig                     2000
#define CatList                     2001
#define WordList                    2005
#define WordScroll                  2010
#define CountField                  2015
#define HistTrig                    2025
#define HistList                    2026
#define PatPush                     2045
#define AnaPush                     2046
#define BldPush                     2047
#define FitPush                     2048
#define QMButton                    2062 // Numbered down from 2065
#define StarButton                  2063
#define VowelButton                 2064
#define ConsButton                  2065
#define InputField                  2070
#define SearchButton                2075
#define ClearButton                 2080
#define WJButton                    2090

/* Controls - Preferences Form */
#define MaxRecTrig                  2500
#define MaxRecList                  2510
#define PreserveListChk             2520
#define ShowAlphaChk                2530
#define ShowSymbolsChk              2540
#define HighlightWCMatchChk         2550
#define PrefsOKButton               2590
#define PrefsCancelButton           2595

/* Controls - Word Judge Form */
#define WJField1                    3000
#define WJField2                    3001
#define WJField3                    3002
#define WJField4                    3003
#define WJField5                    3004
#define WJJudgeButton               3085
#define WJClearButton               3090
#define WJDoneButton                3095

/* Controls - Fit Pattern Form */
#define FitLabel                    4000
#define FitField                    4010
#define FitSearchButton             4020
#define FitClearButton              4030
#define FitAbortButton              4040

/* String/StringTables */
#define InstStr                     9000
#define ModeAdjST                   9010
#define InpHistStr                  9020
#define ProgTextST                  9030
#define RulingST                    9040
#define VersionStr                  9050
#define FitTextST                   9060

/* End of file:  resource.h */
