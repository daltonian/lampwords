/*****************************************************************************
*
* lw.c - LAMPWords - An anagramming & word-finding tool for Palm OS handhelds.
* Copyright 2001-2002, 2007 Paul J. Sidorsky
*
* Current Version:  X  (April 1st, 2007)
*   - See HISTORY file for changes and additions.
*   - Major source changes not covered by HISTORY entries are listed below.
*
* Major source modifications from 1.1 to X:
*   - Resources standardized & cleaned up as much as possible.
* 
* Major source modifications from 1.0 to 1.1:
*   - Moved dict. dbid to global lwdbid and added global lwdbcard.
*   - About dialog is now a CustomAlert so a version string can be used.
*   - LISTSIZE constant added to make modifying scrolling easier.
*   - Constants added for wild card characters.  (JLD)
*   - Cosmetic source cleanups, including some inconsistencies I missed before.
*
* Requires PRC-Tools 2.0 or above.
* Requires Palm OS SDK 4.0 or above.
*
* Acknowledgements:
*   - Jonn Dalton (JLD) contributed the code for Alphagrams as well as the
*     vowel, consonant, and point-value wildcards.
*
* Terms:
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
******************************************************************************/

/* Errorchecking level.  Set to ERROR_CHECK_FULL for debugging.  This will
   enable debug messages which can be seen inside the emulator.  Set to
   ERROR_CHECK_NONE for release versions. */
/* #define ERROR_CHECK_LEVEL   ERROR_CHECK_FULL */
#define ERROR_CHECK_LEVEL   ERROR_CHECK_NONE

/* Newer SDK versions need this to let us get at ListType's members,
   which we do to get the top displayed item on pre-4.0 handhelds. */
#define ALLOW_ACCESS_TO_INTERNALS_OF_LISTS

#include <PalmOS.h>

#include "resource.h"

/*****************************************************************************/
/* Program Constants.                                                        */
/*****************************************************************************/

/* Maximum number of cards supported.  So far, Palm devices only support
   one card slot.  NOTE:  Do not count card 0 (the onboard memory) in the total
   below. */
#define MAXCARDS                1

/* Field sizes. */
#define MAXINPUTLENGTH         15
#define COUNTFIELDMEMSIZE      10

/* Miscellaneous limitations. */
/* Number of items in the Input History. */
#define HISTORYSIZE           100
/* Number of nodes to search before returning control back to the event loop.
   Increase this number for faster searching and slower response times,
   decrease it for slower searching but faster response.  See the search
   functions below for more information on why this is needed. */
#define NODESPERSEARCH        500

/* Program IDs. */
/* NOTE:  All creator IDs used by LAMPWords are registered to the original
          author on palmos.com.  If you make significant changes to LAMPWords
          or use it to derive another work, please be courteous and register
          and use new creator IDs.  Remember, only one program with a given
          creator ID can exist on a device! */
#define CREATORID          'psLW'
#define LWSTATEID               0
#define LWPREFSID               1

/* State & Preference information store using the Palm's Preferences
   facility.  These version numbers have nothing to do with the program
   version number.  Instead, they're just incremented by 1 every time the
   LWState or LWPrefs structures are changed. */
#define LWSTATEVERSION          5
#define LWPREFSVERSION          4

/* List database IDs. */
#define LISTDBCREATORID    'psLL'
#define LISTDBID           'data'

/* List database limitations. */
#define MAXLISTDBRECNODES    2000
#define MAXLISTDBRECS          16

/* Word database parameters. */
#define DBMAGICSTRING      "LW10"  /* NOTE:  Last DB revision was for 1.0. */
#define DBRECORDLENGTH       8192

/* Word database limitations. */
#define MAXCATEGORIES           8
#define MAXCHARS               64

/* Number of rows in results list box (used for scrolling). */
#define LISTSIZE                8
/* Don't show alphagrams of words longer than this (due to lack of space). */
#define MAXALPHALENGTH         10

/* the number of special characters in input; such as blank, vowel,
   etc. (JLD) */
#define MAXSPECIAL 13

/* Special character constants.  (JLD) */
#define CHAR_BLANK           chrQuestionMark
#define CHAR_CONSONANT       chrLessThanSign
#define CHAR_VOWEL           chrCircumflexAccent
#define CHAR_ONE             '1'
#define CHAR_TWO             '2'
#define CHAR_THREE           '3'
#define CHAR_FOUR            '4'
#define CHAR_FIVE            '5'
#define CHAR_SIX             '6'
#define CHAR_SEVEN           '7'
#define CHAR_EIGHT           '8'
#define CHAR_NINE            '9'
#define CHAR_TEN             '0'

/* Other significant characters.  These are NOT considered part of the "special
   characters" list defined above! */
#define CHAR_STAR            chrAsterisk         /* Pun intended :-) */

/* Search modes (not including modeUnknown). */
#define NUMMODES                4
enum
{
    modeUnknown, modePattern, modeAnagram, modeBuild, modeFit
};

/* Word judging limitations. */
#define NUMWJWORDS              5

/****************************************************************************/
/* Data Structures.                                                         */
/****************************************************************************/

/* Terminology and naming notes:

   - The database with all the words is called the word database or
   dictionary database or sometimes even just 'the database'.  Variables
   associated with it are usually prefixed with just "db".

   - The database that stores the search results is called the list database
   or results database.  Its variables usually have "listdb" or "dblist" in
   front.
*/

/* Word database information. */
typedef struct
{
    /* Magic string to identify the database.  Used so we can tell the user
       to install a new dictionary if an old one is present. */
    Char magic[5];
    /* The database's name.  Used so we can tell when the user switches
       to a new dictionary.  Note this isn't displayed anywhere at this
       time. */
    Char name[11];
    /* Descriptive info, shown to the user on demand. */
    Char title[41];
    Char desc[41];
    Char author[41];
    Char extra[41];
    /* Character translation info. */
    Char ch[MAXCHARS];
    UInt8 numchars;
    /* Category information. */
    Char catsym[MAXCATEGORIES];
    Char catname[MAXCATEGORIES][11];
    Char catinclude[MAXCATEGORIES][MAXCATEGORIES];
    UInt8 numcategories;
    /* Total number of nodes in the database. */
    UInt32 numnodes;
} DBHeader;

/* Word database node.  Bit fields make the manipulation easier, although
   it does complicate generating the database a bit. */
typedef struct
{
    UInt8 endoflist : 1;
    UInt8 endofword : 1;
    Char letter : 6;      /* To display, use the header's translation info. */
    UInt8 category : 3;   /* Only defined when endofword is true. */
    UInt32 child : 21;    /* 0 indicates no child. */
} DBNode;

/* List database item.  See the search functions below. */
typedef struct
{
    Char word[MAXINPUTLENGTH + 1];
    Char catsym;
} DBListItem;

/* Program state information.  Stored in the device's Perference database
   so that LAMPWords can restore its appearance when it is stopped and
   restarted. */
typedef struct
{
    UInt8 searchmode;
    Char inputfield[MAXINPUTLENGTH + 1];
    Char inputhistory[HISTORYSIZE][MAXINPUTLENGTH + 1];
    UInt16 wordcount;
    UInt16 topitem;
    UInt8 category;
    Char dictname[11];
} LWState;

/* Program preferences.  Also stored in the Preference database.  The only
   difference between this info and the state info is that the preference
   info is backed up to the PC during a HotSync while the state information is
   not.  The gist is that the program's behaviour should be preserved but the
   appearance doesn't need to be. */
typedef struct
{
    UInt8 listrecs;
    Boolean preservelist;
    Boolean showAlphagram;
    Boolean showSymbols;
    Boolean highlightMatches;
} LWPrefs;

/*****************************************************************************/
/* Global Variables.                                                         */
/*****************************************************************************/

/* Externalized string buffers. */
static Char modeadj[NUMMODES + 1][21];
static Char histstr[21];
static Char progtext[2][21];
static Char version[11];
static Char fittext[2][21];

/* Tells us which form we're using. */
static FormPtr curform = NULL;

/* NOTE:  Only controls that need to be manipulated are listed below. */

/* Controls for Main Form. */
static ListPtr wordlist = NULL;
static ControlPtr patpush = NULL;
static ControlPtr anapush = NULL;
static ControlPtr bldpush = NULL;
static ControlPtr fitpush = NULL;
static FieldPtr inputfield = NULL;
static FieldPtr countfield = NULL;
static ControlPtr searchbut = NULL;
static ScrollBarPtr scrollbar = NULL;
static ListPtr histlist = NULL;
static ControlPtr histtrig = NULL;
static ListPtr catlist = NULL;
static ControlPtr cattrig = NULL;

/* Controls for Preferences Form. */
static ListPtr maxreclist = NULL;
static ControlPtr maxrectrig = NULL;
static ControlPtr preslistchk = NULL;
static ControlPtr showalphachk = NULL;
static ControlPtr showsymchk = NULL;
static ControlPtr highlightmatchchk = NULL;

/* Controls for Word Judge Form. */
static FieldPtr wjwordfield[NUMWJWORDS] =
    { NULL, NULL, NULL, NULL, NULL };

/* Controls for Fit Form. */
static FieldPtr fitlabel = NULL; /* Actually a field, treated as a label. */
static FieldPtr fitfield = NULL;
static ControlPtr fitsearchbut = NULL;

/* Progress Box.  Not supported on 2.0 devices, but we'll detect that on
   startup and set useprog accordingly. */
static ProgressPtr progbox = NULL;
static Boolean useprog = false;

/* Similarly, LstGetTopItem() is supported only on 4.0 and higher devices. */
static Boolean usegettop = false;

/* Text buffer for Preferences Form. */
static Char maxrecbuf[6];

/* Variables for accessing the word database. */
static LocalID lwdbid = 0;   /* Main DB ID. */
static UInt16 lwdbcard = -1;
static DmOpenRef lwdb = 0;   /* Main DB handle. */
static MemHandle* dbrec;
static DBNode** dbnodeptr;
static UInt8** dbbyteptr; /* Same as dbnodeptr; used for size calculations. */
static MemHandle dbhdr;
static DBHeader* dbhdrptr;
static UInt16 numrecs;

/* Variables for accessing the list database. */
static DmOpenRef listdb = 0;      /* Main List DB handle. */
static UInt16 listdbcard = 0;
static LocalID listdbid = 0;
static MemHandle listdbrecmem[MAXLISTDBRECS];
static DBListItem* listdbrecptr[MAXLISTDBRECS];

/* Variables used while building the list database. */
static UInt16 curlistdbofs = 0;
static UInt16 curlistdbrec = 0;

/* Pointer lists used when passing to LstSetListChoices().  The strings
   are contained elsewhere in memory, we just need to build a list of
   pointers to them. */
static Char* catnames[MAXCATEGORIES];
static Char* histlistitems[HISTORYSIZE];

/* Locale-specific variables. */
static Char thsep;      /* Thousands separator. */
static Char decsep;     /* Decimal separator. */

/* State & Preference variables. */
static LWState state;
static LWPrefs prefs;

/* Search information.  This is all global because the search functions
   have to yield control to the event loop from time to time and and pick up
   where they left off.  See below for more information. */
static UInt8 indent = 0;     /* Current position inside the search word. */
static DBNode* dbstartnode;  /* Node to begin the search at. */
static Char searchword[MAXINPUTLENGTH + 1]; /* The actual input word itself. */
static UInt8 searchwordlen;  /* Length of searchword, stored for speed. */
static Char searchpat[MAXINPUTLENGTH + 1];  /* Fit mode search pattern. */
static UInt8 searchpatlen;   /* Length of searchpat, stored for speed. */
static Char searchbuf[MAXINPUTLENGTH + 1];  /* The matched (output) word. */
static UInt8 searchmode;     /* Which mode was selected. */
static void (*searchfunc)(); /* Search function to be used. */
static Char origtext[MAXINPUTLENGTH + 1];   /* Original search pattern. */
static Char origpat[MAXINPUTLENGTH + 1];    /* Original fit pattern. */
static Char* textptr;        /* Pointer to the actual input field text. */
static Boolean searching = false;           /* Are we searching? */
static Boolean searchfull = false;          /* Did we run out of room? */
/* keep track of special characters such as blanks, vowels, consonants, etc. */
static Boolean isSpecial[MAXSPECIAL][MAXINPUTLENGTH];
/* number of special character used; per each type */
static UInt8 numSpecial[MAXSPECIAL];
static UInt8 numchars[MAXCHARS];  /* Count of chars in the search pattern. */
static DBNode* stemnodes[MAXINPUTLENGTH]; /* 'Parent' node at each level. */
static UInt8 stemrecs[MAXINPUTLENGTH];    /* 'Parent' record at each level. */
static DBNode* searchnode;        /* Current node being examined. */
static UInt16 searchrec;          /* Current record being examined. */
static Boolean wcmatchpos[MAXINPUTLENGTH]; /* If each char matches ? or *. */
/* Variables for Pattern Mode. */
static UInt8 searchpos;           /* Current position in input word. */
static Int8 prevstar[MAXINPUTLENGTH + 1];  /* Loc. of prev. * from any pos. */
static Boolean onstar[MAXINPUTLENGTH];    /* Does each spot point to a star? */
static Int8 firststar;            /* Location of first star in string. */
static Int8 laststar;             /* Location of final star in string. */
static UInt8 searchmatchpos[MAXINPUTLENGTH]; /* String pos. at each level. */
/* Variables for Anagram Mode. */
static Boolean starmatch[MAXINPUTLENGTH]; /* Positions of * matches. */
static UInt8 numstars;            /* Number of * wildcards found. */
static UInt8 numnonstars;         /* Number of other characters found. */
static UInt8 numnonstarmatches = 0;       /* Number of * matches. */

/* letters making up a special type; ignore blank since that matches
   everything.  (JLD) */
/* NOTE:  Order must correspond to that of specialChar array, below. */
/* NOTE:  Pools should be listed in decreasing order of size. */
/* TODO:  Move this to the dictionary next time the format changes, to
          allow for internationalization. */
static Char* specialPool[MAXSPECIAL] =
{
    "",                 /* Blank - matches anything; handled specially. */
    "BCDFGHJKLMNPQRSTVWXYZ",
    "AEILNORSTU",
    "AEIOU",
    "FHVWY",
    "BCMP",
    "DG",
    "JX",
    "QZ",
    "K",
    "",                 /* Unused point-values. */
    "",
    ""
};

/* input characters representing special wild card characters.  (JLD) */
/* NOTE:  Order must correspond to that of specialPool array, above. */
static Char specialChar[MAXSPECIAL] =
{
    CHAR_BLANK,
    CHAR_CONSONANT,
    CHAR_ONE,
    CHAR_VOWEL,
    CHAR_FOUR,
    CHAR_THREE,
    CHAR_TWO,
    CHAR_EIGHT,
    CHAR_TEN,
    CHAR_FIVE,
    CHAR_SIX,
    CHAR_SEVEN,
    CHAR_NINE
};

/* Word Judge variables. */
/* The words to be judged. */
static Char wjwords[NUMWJWORDS][MAXINPUTLENGTH + 1];
static Char rulingtxt[2][21];     /* Text displayed for each ruling. */

/* Buffer to build the dictionary information from the header.  We need this
   because Custom Alerts only support 3 parameters but the dictionary info
   has more than 3. */
static Char dictinfo[3][81];

/*****************************************************************************/
/* Function Prototypes.                                                      */
/*****************************************************************************/

/* Functions are listed in the order they are defined in this file. */
static void DrawWord(Int16 itemnum, RectanglePtr bounds, Char** itemsText);
static Boolean AddWordToListDB(UInt8 cat);
static void SetFieldText(FieldPtr field, Char* text, UInt16 memsize);
static void AddNumSepToStr(Char* str);
static void CleanUpStr(Char* str, Boolean wj);
static void ConvStr(Char* str);
static Char* GenAlphagram(Char* word);
static Boolean IsSpecialChar(Char c, UInt8 type);
static UInt16 GetSearchMode(void);
static void StartSearch(UInt8 mode, Boolean resumefit);
static void DoSearch(void);
static void EndSearch(void);
static Boolean SearchProgFunc(PrgCallbackDataPtr cbP);
static void SearchPattern(void);
static void SearchAnagram(void); 
static void SearchBuild(void);
static void ZeroCounts(UInt8 index);
static Boolean FitPattern(Boolean usingStar, Char* searchPattern, Char* searchBuffer);
static Boolean WordJudge(void);
static Err WriteDBData(const void* dataP, UInt32* sizeP, void* userDataP);
static Err SendDatabase(UInt16 cardNo, LocalID dbID, Char* nameP,
                        Char* descriptionP);
static Err SendMe(void);
static Err StartApplication(void);
static void StopApplication(void);
static Boolean LWFormEventHandler(EventPtr event);
static Boolean PrefsFormEventHandler(EventPtr event);
static Boolean WJFormEventHandler(EventPtr event);
static Boolean FitFormEventHandler(EventPtr event);
static Boolean AppEventHandler(EventPtr event);
static void EventLoop(void);
UInt32 PilotMain(UInt16 launchCode, void* cmdPBP, UInt16 launchFlags);

/*****************************************************************************/
/* Function definitions.                                                     */
/*****************************************************************************/

/*****************************************************************************/
/* Utility Functions.                                                        */
/*****************************************************************************/

/* DrawWord() - Draws a word in the search results list box.  (Callback)
   Parameters:  See Palm OS documentation for LstSetDrawFunction().
   Returns:  Nothing.
   Notes:  Not called by LAMPWords.  This is a callback function used when
           the search results are being displayed.
           We could set up a list of pointers inside the word database to
           pass to LstSetListChoices(), but this would take extra time and use
           more space, and we're already using a lot of space and taking a lot
           of time as it is. */
static void DrawWord(Int16 itemnum, RectanglePtr bounds, Char** itemsText)
{
    /* Storage buffer.  This is static because the function is called
       a lot and we don't need to waste time allocating space on the stack
       for this each time a call takes place. */
    static Char tmp[MAXINPUTLENGTH*2 + 1 + 1 + 3];
    DBListItem* itemptr;     /* The item being drawn. */
    Int16 size;
    Int16 wordlength;        /* Stored for speed. */

    if (searching)
        return;

    /* Safeguard in case somehow the list, scrollbar, and the results get out
       of sync. */
    if (itemnum >= state.wordcount)
    {
        return;
    }

    itemptr = &listdbrecptr[itemnum / MAXLISTDBRECNODES]
                           [itemnum % MAXLISTDBRECNODES];

    size = MAXINPUTLENGTH + 1 + 1;
    wordlength = StrLen(itemptr->word);
     
    if (prefs.showAlphagram && wordlength <= MAXALPHALENGTH)
    {
        size += MAXINPUTLENGTH + 3;
    }

    MemSet(tmp, size, 0);

    /* Append the alphagram.  (JLD) */
    if (prefs.showAlphagram && wordlength <= MAXALPHALENGTH)
    {
        StrCat(tmp, GenAlphagram(itemptr->word));
        StrCat(tmp, " / ");
    }

    StrCat(tmp, itemptr->word);
    if (prefs.showSymbols)
    {
        tmp[StrLen(tmp)] = itemptr->catsym;
    }
    
    WinDrawChars(tmp, StrLen(tmp),
                 bounds->topLeft.x, bounds->topLeft.y);

}

/*****************************************************************************/

/* AddWordToListDB() - Adds a word to the list database (search results).
   Parameters:  cat - The category the word belongs to.
   Returns:  true on success, false on failure.
   Notes:  The word being added is assumed to be contained in the
           global variable searchbuf. */
static Boolean AddWordToListDB(UInt8 cat)
{
    DBListItem tmpitem; /* Buffer to hold the new word info. */
    Char* p;            /* Destination pointer. */
    Char* s;            /* Source pointer. */
    Int8 i;
    
    /* I don't think this is needed anymore.  There was a time where it was
       needed, when I was experimenting with different search algorithms.
       Anyhow it's here just in case. */
    if (searchfull)
        return false;

    /* Copy the actual character to be displayed.  We could copy the character
       codes but then the display callback would have to translate and
       displaying would be slower. */
    for (i = 0, s = searchbuf, p = tmpitem.word; *s; i++, s++, p++)
    {
        *p = dbhdrptr->ch[(UInt8) *s];
        if (wcmatchpos[i] && prefs.highlightMatches)
            *p += 32; /* Make letter lower case. */
    }
    *p = '\0';
    
    tmpitem.catsym = dbhdrptr->catsym[cat];

    DmWrite(listdbrecptr[curlistdbrec], curlistdbofs, &tmpitem,
            sizeof(DBListItem));

    state.wordcount++;
    curlistdbofs += sizeof(DBListItem);

    /* If we fill up this list DB record, try to make a new one. */
    if (!(state.wordcount % MAXLISTDBRECNODES))
    {
        curlistdbrec++;
        if (curlistdbrec == prefs.listrecs)
        {
            FrmAlert(ListFullAlert);
            return false;
        }
        curlistdbofs = 0;
        listdbrecmem[curlistdbrec] = DmNewRecord(listdb, &curlistdbrec,
                              MAXLISTDBRECNODES * sizeof(DBListItem));
        if (listdbrecmem[curlistdbrec] == 0)
        {
            prefs.listrecs = curlistdbrec;
            FrmAlert(ListDBAlert);
            return false;
        }
        listdbrecptr[curlistdbrec] =
            (DBListItem*) MemHandleLock(listdbrecmem[curlistdbrec]);
    }

    return true;
}

/*****************************************************************************/

/* SetFieldText() - Sets the contents of a text field.
   Parameters:  field - Pointer to the field control to set.
                text - The text to set.
                memsize - Maximum size of the field's buffer.
   Returns:  Nothing.
   Notes:  Unfortunately it is very cumbersome to set a field's text
           programmatically.  You'd think they could just provide a function
           for it, but no such luck.  This function fills that void.
           We need the maximum size because each time we set the text we
           might have to reallocate the memory. */
static void SetFieldText(FieldPtr field, Char* text, UInt16 memsize)
{
    MemHandle mem;
    Char* ptr;

    mem = FldGetTextHandle(field);
    /* The handle can be null if it's the first time we're changing the
       text, or if the field is empty. */
    if (mem == NULL)
    {
        mem = MemHandleNew(memsize);
    }

    /* Yes, this is the negation of the above test, but we have to test
       a newly-allocated handle too so we can't use an else here. */
    if (mem != NULL)
    {
        /* Last-ditch attempt to make this work. */
        if (MemHandleSize(mem) < memsize)
        {
            if (MemHandleResize(mem, memsize) != 0)
            {
                ErrDisplay("Can't resize field handle for text set.");
                mem = NULL;
            }
        }
    }
    else
    {
        ErrDisplay("Can't create field handle for text set.");
    }

    /* Hey, it's that same test again!  We need it, though, because we might
       have lost the handle if an error occured. */
    if (mem != NULL)
    {
        /* This is straight out of the Palm OS Reference Manual. */
        FldSetTextHandle(field, NULL);
        ptr = (Char*) MemHandleLock(mem);
        if (ptr != NULL)
        {
            StrCopy(ptr, text);
            MemHandleUnlock(mem);
        }
        FldSetTextHandle(field, mem);
        FldSetInsertionPoint(field, StrLen(text));
        FldDrawField(field);
    }

}

/*****************************************************************************/
/* String / Character Functions.                                             */
/*****************************************************************************/

/*****************************************************************************/

/* AddNumSepToStr() - Inserts numeric separators into an unformatted string.
   Parameters:  sp - The string to insert the separators into.
   Returns:  Nothing.  The string pointed to by sp will be modified.
   Notes:  It's assumed sp is a buffer big enough to hold the modified string,
           which will be larger than the original.  Also, the modified string
           can't be longer than 20 characters long (an arbitrary
           restriction). */
static void AddNumSepToStr(Char* sp)
{
    Char tmp[21];
    UInt8 i = 0;
    Char* tp = tmp;
    UInt8 ofs = StrLen(sp) % 3;

    if (StrLen(sp) <= 3) return;

    StrCopy(tmp, sp);

    /* All we're doing here is sticking commas in the appropriate places.
       This is done from left to right, with the ofs variable telling us
       where we need to stick the first comma. */
    while (*tp)
    {
        if (i % 3 == ofs && i > 0)
        {
            *(sp++) = ',';
        }
        *(sp++) = *(tp++);
        i++;
    }

    *sp = '\0';

}

/*****************************************************************************/

/* CleanUpStr() - Filters out any unsupported characters from a string.
   Parameters:  str - The string to filter.  This should be a search
                      pattern entered by the user.
                wj - Whether or not we're in word judge mode.  If we are
                     then we'll also filter out wild card characters.
   Returns:  Nothing.  The string pointed to by str will be modified.
   Notes:  The filtered string will never be longer than the original, but an
           arbitrary limit is imposed.
           IMPORTANT - This function also updates the numstars, numnonstars,
                       firststar, laststar, prevstar, and onstar globals. */
static void CleanUpStr(Char* str, Boolean wj)
{
    Char tmp[MAXINPUTLENGTH + 1];      /* Holds the new string. */
    Char* s = tmp;                     /* Pointer to traverse new string. */
    Char* t = str;                     /* Pointer to traverse old string. */
    UInt8 d;                           /* Loop counter. */
    /* How many * wildcards are allowed in each mode.  (Mode 0 is unused.) */
    UInt8 maxstars[NUMMODES + 1] = { 0, MAXINPUTLENGTH, 1, 0, 0 };

    MemSet(onstar, MAXINPUTLENGTH, false);
    MemSet(prevstar, MAXINPUTLENGTH + 1, -1);
    numstars = 0;
    numnonstars = 0;
    firststar = -1;
    laststar = -1;
    /* Selectively copy each character. */
    for (; *t; t++)
    {
        if (*t >= 'A' && *t <= 'Z')
        {
            *s++ = *t;
            numnonstars++;
        }
        /* It's assumed dictionary databases use upper-case characters. */
        else if (*t >= 'a' && *t <= 'z')
        {
            *s++ = *t - 32;
            numnonstars++;
        }
        else if ((*t == ' ' || *t == '.' || *t == CHAR_BLANK) && !wj)
        {
            *s++ = CHAR_BLANK;
            numnonstars++;
        }
        else if ((*t == '\'' || *t == CHAR_VOWEL) && !wj)
        {
            *s++ = CHAR_VOWEL;
            numnonstars++;
        }
        else if ((*t == '-' || *t == CHAR_CONSONANT) && !wj)
        {
            *s++ = CHAR_CONSONANT;
            numnonstars++;
        }
        else if ((*t >= CHAR_TEN && *t <= CHAR_NINE) && !wj)
        {
            *s++ = *t;
            numnonstars++;
        }
        else if ((*t == '/' || *t == CHAR_STAR) && !wj)
        {
            Boolean foundprev = false;

            /* Consecutive stars have the same meaning as one star, so if
               we just copied one we won't copy another. */
            if (s > tmp)
            {
                if (*(s - 1) == CHAR_STAR)
                    foundprev = true;
            }
            if (!foundprev && numstars < maxstars[searchmode])
            {
                laststar = (UInt8) (s - tmp);
                if (firststar < 0)
                    firststar = laststar;
                onstar[laststar] = true;
                MemSet(&prevstar[laststar], MAXINPUTLENGTH - laststar + 1,
                       laststar);
                *s++ = CHAR_STAR;
                numstars++;
            }
        }
        else
        {
            /* If we don't find the character, look to see if the dictionary
               uses it.  If not it won't be copied. */
            /* TODO:  Test this!!! */
            for (d = 1; d < dbhdrptr->numchars; d++)
            {
                if (*t == dbhdrptr->ch[d])
                {
                    *s++ = *t;
                    numnonstars++;
                    break;
                }
            }
        }
    }

    *s = '\0';
    StrCopy(str, tmp);

}

/*****************************************************************************/

/* ConvStr() - Converts a string from actual characters to character codes.
   Parameters:  str - The string to filter.  This should be a search
                      pattern entered by the user.
   Returns:  Nothing.  The string pointed to by str will be modified.
   Notes:  The converted string is always the same length as the original.
           Character codes range from 1 to 63; the actual characters used
           are stored in the dictionary database header.  Blanks (question
           marks) are NOT converted so they can be used by the search
           functions.  */
static void ConvStr(Char* str)
{
    UInt8 d;       /* Counter. */

    /* Look for each character and convert it. */
    for (; *str; str++)
    {
        if (*str != CHAR_BLANK && *str != CHAR_VOWEL &&
            *str != CHAR_CONSONANT && *str != CHAR_STAR &&
            (*str < CHAR_TEN || *str > CHAR_NINE))
        {
            /* 0 is a reserved code, so the search starts at 1. */
            for (d = 1; d < dbhdrptr->numchars; d++)
            {
                if (*str == dbhdrptr->ch[d])
                {
                    *str = d;
                    break;
                }
            }
            /* Safeguard in case we don't find a character. */
            if (d == dbhdrptr->numchars)
            {
                ErrDisplay("Problem converting string - check the database.");
                *str = 0;  /* Safety setting. */
            }
        }
    }

}

/*****************************************************************************/

/* GenAlphagram() - Puts the letters of a word in alphabetical order.  (JLD)
   Parameters:  word - the String to alphabetize
   Returns:  the alphabetized word */
static Char* GenAlphagram(Char* word)
{
    Int16 n,i,j,size;
    Char tmp;
    static Char buffer[MAXINPUTLENGTH + 1];

    size = MAXINPUTLENGTH + 1;
    MemSet(buffer, size, 0);
    StrCat(buffer, word);

    n = StrLen(word);

    /* simple selection sort algorithm */
    for (i=0; i<n-1; i++)
    {
        for (j=i+1; j<n; j++)
        {
            if (buffer[i] > buffer[j])
            {
                tmp = buffer[i];
                buffer[i] = buffer[j];
                buffer[j] = tmp;
            }
        }
    }

    return buffer;
}

/*****************************************************************************/

/* IsSpecialChar() - Checks if a character is a "special" character.  (JLD)
   Parameters:  c - the Char to check.  
                type - the type of special character to look for (vowel, blank,
                       etc.)
   Returns:  true if the character is special, false otherwise
   Notes:  Special characters are generally wildcards.  The lists are
           defined near the top of this file.
           It's assumed dictionary databases use upper-case characters. */

static Boolean IsSpecialChar(Char c, UInt8 type)
{ 
    /* type 0 is blank and always matches */
    return (type == 0 || NULL != StrChr(specialPool[type], c+'A'-1));
}

/*****************************************************************************/
/* Search-Related Functions.                                                 */
/*****************************************************************************/

/* GetSearchMode() - Determines which search mode has been selected.
   Parameters:  None.
   Returns:  A constant indicating the current search mode.
   Notes:  This is just a convenience function to detect which push button
           is selected. */
static UInt16 GetSearchMode(void)
{

    if (CtlGetValue(patpush) == 1)
        return modePattern;
    if (CtlGetValue(anapush) == 1)
        return modeAnagram;
    if (CtlGetValue(bldpush) == 1)
        return modeBuild;
    if (CtlGetValue(fitpush) == 1)
        return modeFit;

    return modeUnknown;
}

/*****************************************************************************/

/* StartSearch() - Begins a new search.
   Parameters:  mode - The search mode to be used.
                resumefit - Whether we're resuming a fit search after
                            obtaining the pattern.  If true, mode is ignored.
   Returns:  Nothing. */
static void StartSearch(UInt8 mode, Boolean resumefit)
{
    Err res;                 /* Result code. */
    DmSearchStateType ds;    /* Not used; needed by a Palm OS call. */
    UInt16 d;                /* Counter. */
    UInt16 inphistloc;       /* Where we found the string in the history. */

    /* Yes, it's a dreaded goto!  As a former BASIC programmer, they make me
       cringe, but it was either this or divide up a crucial section of the
       program, so right now I'm taking the easy way out. */
    /* TODO:  Eliminate this goto by finding a better way to do (or at least
              fake) a modal dialog (see below at target label). */
    if (resumefit)
        goto resume_fit_search;

    searchmode = mode;

    switch (searchmode)
    {
    case modePattern:
        searchfunc = SearchPattern;
        break;

    case modeAnagram:
        searchfunc = SearchAnagram;
        break;

    /* Build mode and Fit mode use the same search function. */
    case modeBuild:
    case modeFit:
        searchfunc = SearchBuild;
        break;

    default:
        searchfunc = NULL;
    }

    if (searchfunc != NULL)
    {
        textptr = FldGetTextPtr(inputfield);
        if (textptr != NULL)
        {
            /* Copy the search pattern. */
            MemSet(searchword, MAXINPUTLENGTH + 1, 0);
            StrCopy(searchword, textptr);
            textptr = NULL;

            CleanUpStr(searchword, false);
            if (searchword[0] == '\0')
                return;

            /* origtext is kind of misnamed.  It actually contains the
               filtered string which is then placed back into the input
               field. */
            StrCopy(origtext, searchword);
            SetFieldText(inputfield, origtext, MAXINPUTLENGTH + 1);
            FldSetSelection(inputfield, 0, StrLen(origtext));

            /* Add the input to the input history.  We don't duplicate items,
               but we will promote an item to the top of the list if it's
               already there. */
            inphistloc = HISTORYSIZE - 1; /* See below. */
            for (d = 0; d < HISTORYSIZE; d++)
            {
                if (!StrCompare(origtext, state.inputhistory[d]))
                {
                    inphistloc = d;
                    break;
                }
            }

            /* Once we find the item we move everything in front of
               it down one place.  Note that we move everything if the item's
               in the last spot, which is also what we want if the item isn't
               found.  This explains the initial setting of inphistloc above.
               From our point of view we don't care whether or not the item was
               found or is in the last spot, because after the shifting we copy
               whatever's in the input to the first spot, and it'll look right
               to the user.  (Sorry for my poor explanation!) */
            for (d = inphistloc; d > 0; d--)
            {
                StrCopy(state.inputhistory[d],
                        state.inputhistory[d - 1]);
            }
            StrCopy(state.inputhistory[0], origtext);

            ConvStr(searchword);
            searchwordlen = StrLen(searchword);

            /* Special prepatory stuff needed by the more complex modes. */
            if (searchmode != modePattern)
            {
                UInt8 i = 0;
                for (i=0; i<MAXSPECIAL; i++)
                {
                    numSpecial[i] = 0;
                    MemSet(isSpecial[i], MAXINPUTLENGTH, 0);
                }
                MemSet(numchars, MAXCHARS, 0);

                /* Count occurrences of each character. */
                for (d = 0; d < searchwordlen; d++)
                {
                    UInt8 i = 0;
                    for (i=0; i<MAXSPECIAL; i++)
                    {
                        if (searchword[d] == specialChar[i])
                        {
                            numSpecial[i]++;
                            break;
                        }
                    }
                    if (i == MAXSPECIAL)
                    {
                        numchars[(UInt8) searchword[d]]++;
                    }
                }
            }
            searchpos = 0;

            MemSet(searchpat, MAXINPUTLENGTH + 1, 0);
            searchpatlen = 0;

            MemSet(starmatch, MAXINPUTLENGTH, 0);
            numnonstarmatches = 0;

            MemSet(searchmatchpos, MAXINPUTLENGTH, 0);

            MemSet(wcmatchpos, MAXINPUTLENGTH, 0);

resume_fit_search:
            if (searchmode == modeFit)
            {
                /* Unfortunately, I couldn't find a way to both make a modal
                   dialog and control the input to do on-the-fly character
                   translation.  So I fake it by opening a non-modal form and
                   returning.  When the other form closes it will call
                   StartSearch() again with resumefit set and we'll wind up
                   in the else clause, essentially resuming where we left
                   off. */
                if (!resumefit)
                {
                    FrmPopupForm(FitForm);
                    return;
                }
                else
                {
                    if (searchpatlen == 0)
                        return;
                    StrCopy(origpat, searchpat);
                    ConvStr(searchpat);
                }
            }

            /* Initialization stuff. */
            indent = 0;
            MemSet(searchbuf, MAXINPUTLENGTH + 1, 0);
            state.wordcount = 0;

            /* Next, we set up the search results database.  Because the word
               database is letters only, the words need to be copied as they
               are found.  Databases are the only thing suitable for storing
               large amounts of data, so we create one specifically for the
               results. */

            /* Blow out any old search results. */
            if (listdb != 0)
            {
                for (d = 0; d < MAXLISTDBRECS; d++)
                {
                    if (listdbrecmem[d] != 0)
                    {
                        MemHandleUnlock(listdbrecmem[d]);
                        DmReleaseRecord(listdb, d, true);
                        listdbrecmem[d] = 0;
                        listdbrecptr[d] = NULL;
                    }
                }
                DmCloseDatabase(listdb);
                listdb = 0;
            }
            res = DmGetNextDatabaseByTypeCreator(true, &ds, LISTDBID,
                                                 LISTDBCREATORID, false,
                                                 &listdbcard, &listdbid);
            if (res == errNone)
            {
                DmDeleteDatabase(0, listdbid);
            }

            /* Create a new search results database. */
            res = DmCreateDatabase(0, "LAMPWordsLst", LISTDBCREATORID,
                                   LISTDBID, false);
            if (res != errNone)
            {
                ErrDisplay("Can't create results database.");
                return;
            }

            res = DmGetNextDatabaseByTypeCreator(true, &ds, LISTDBID,
                                                 LISTDBCREATORID, false,
                                                 &listdbcard, &listdbid);
            if (res != errNone)
            {
                ErrDisplay("Problem accessing results database.");
                return;
            }

            listdb = DmOpenDatabase(listdbcard, listdbid, dmModeReadWrite);
            if (listdb == NULL)
            {
                ErrDisplay("Problem opening results database.");
                return;
            }

            /* We start with just one record and will add more later
               if needed. */
            curlistdbofs = 0;
            curlistdbrec = 0;
            listdbrecmem[0] = DmNewRecord(listdb, &curlistdbrec,
                                  MAXLISTDBRECNODES * sizeof(DBListItem));
            if (listdbrecmem[0] == 0)
            {
                FrmAlert(OutOfMemAlert);
                return;
            }
            listdbrecptr[0] = (DBListItem*) MemHandleLock(listdbrecmem[0]);

            /* More initialization. */
            searchfull = false;
            searching = true;
            searchnode = dbstartnode;
            searchrec = 0;

            /* Start the progress dialog if supported. */
            if (useprog)
            {
                progbox = PrgStartDialogV31(progtext[0], SearchProgFunc);
                PrgUpdateDialog(progbox, 0, 0, 0, true);
            }
            else
            {
                /* Hide the Search button to provide a visual cue. */
                FrmHideObject(curform,
                              FrmGetObjectIndex(curform, SearchButton));
                CtlSetUsable(searchbut, false);
            }

            /* We cheat a bit and start the search right away, rather than
               waiting for the event loop to start the search. */
            DoSearch();

        }
    }

}

/*****************************************************************************/

/* DoSearch() - Front-end that performs the actual search.
   Parameters:  None.
   Returns:  Nothing.
   Notes:  Normally called from the event loop when the Palm device isn't
           doing anything else.  This function is a front-end that does some
           checking and calls correct search function. */
static void DoSearch(void)
{

    /* Check for a cancel if progress dialogs are supported. */
    if (useprog)
    {
        if (PrgUserCancel(progbox))
        {
            searching = false;
            EndSearch();
            return;
        }
    }

    searchfunc();

    /* See if the search finished during the last call. */

    if (searchfull)
        searching = false;

    if (!searching)
        EndSearch();

}

/*****************************************************************************/

/* EndSearch() - Performs some cleanup after a search finishes.
   Parameters:  None.
   Returns:  Nothing. */
static void EndSearch(void)
{
    Char textbuf[COUNTFIELDMEMSIZE + 1];

    /* This function is called on program startup as a shortcut to restore
       the results of the last search before exit, so we must make sure
       the progress dialog is actually being used if it's supported (it will
       be NULL if not). */
    if (useprog && progbox != NULL)
    {
        PrgStopDialog(progbox, true);
    }
    /* Reshow the search button on older systems.  It won't hurt if we do this
       even at the start of the program. */
    if (!useprog)
    {
        CtlSetUsable(searchbut, true);
        FrmShowObject(curform, FrmGetObjectIndex(curform, SearchButton));
    }

    /* Show the word count. */
    StrIToA(textbuf, state.wordcount);
    AddNumSepToStr(textbuf);
    StrLocalizeNumber(textbuf, thsep, decsep);
    SetFieldText(countfield, textbuf, COUNTFIELDMEMSIZE + 1);

    /* Setup the list. */
    LstSetListChoices(wordlist, NULL, state.wordcount);
    if (state.wordcount > 0)
        LstSetSelection(wordlist, 0);
    LstDrawList(wordlist);
    SclSetScrollBar(scrollbar, 0, 0,
                    state.wordcount <= LISTSIZE ?
                        0 : state.wordcount - LISTSIZE, LISTSIZE);

}

/*****************************************************************************/

/* SearchProgFunc() - Updates the progress dialog text.  (Callback)
   Parameters:  See Palm OS documentation for PrgStartDialog().
   Returns:  See Palm OS documentation for PrgStartDialog().
   Notes:  Not called by LAMPWords.  This is a callback function used when
           the progress dialog is updated.
           We only update the progress dialog when it's first opened.  After
           that no changes are made.  However, the Palm OS doesn't seem to
           support any other way to change the text so we have to have this
           callback even though it's only used once. */
static Boolean SearchProgFunc(PrgCallbackDataPtr cbP)
{
    Char buf[31];

    buf[0] = '\0';
    if (searchmode == modeFit)
    {
        StrPrintF(buf, "\n%s%s", fittext[1], origpat);
    }

    StrPrintF(cbP->textP, "%s:\n%s%s\n%s",
              modeadj[searchmode], origtext, buf, progtext[1]);

    return true;
}

/*****************************************************************************/
/* Search Functions.                                                         */
/*****************************************************************************/

/* NOTES:

   The Search Functions perform the actual "grunt work" of searching for
   words.

   Palm OS is not a multitasking OS, and processes cannot be interrupted by
   most actions of the user.  Searches may take minutes, so if the
   search functions were written as standard do-everything-and-then-return
   functions, the user's Palm would be rendered completely unusable.  Even the
   off button wouldn't work (at least on some devices)!  This is how the first
   prerelease version of LAMPWords worked (and its searches took even longer).

   So the search functions are designed as follows:

   - Any variable that needs to be preserved through more than one
     iteration of the search loop is global.

   - As a result of the above, the search function can stop after any
     iteration of the search loop and pick up where it left off later on.

   - To facilitate this, the algorithms are iterative algorithms designed to
     mimic recursion.  This is easy since the maximum number of recursion
     levels is finite and small (specifically, MAXINPUTLENGTH).

   - Initialization of the search is done external to the search function,
     specifically in the StartSearch() function.

   - A small number of iterations is run each time the search function is
     called.  This means nearly every search will require multiple calls.
     (Those that don't won't take long enough to matter.)

   - A front-end function, DoSearch(), is used instead of having the event
     loop call the search function directly.  This allows common logic
     (mostly cancellation and termination detection) to run before and after
     each call.  DoSearch() calls the actual search function below.

   - A global flag is set telling the event loop that we're currently doing a
     search.  When this is set the event loop won't wait for an event and
     will instead call DoSearch() if nothing happens.  See the event loop
     below for more info.

   - The upshot of all of this is that the search can be cancelled, the device
     turned off, and the program can even be exited during a search.
*/

/*****************************************************************************/

/* SearchPattern() - Performs a search in Pattern (PAT) mode.
   Parameters:  None.
   Returns:  Nothing.
   Notes:  The pattern search simply tries to match the input in the
           order given. */
static void SearchPattern(void)
{
    UInt16 count = 0;        /* Counts the number of iterations. */
    Boolean match = false;   /* Did we match a character? */
    UInt8 i;                 /* Loop counter. */

    /* The pattern search algorithm is the simplest.  It's basically
       a recursive graph walk, except that it's been converted to behave
       iteratively as noted above.  We don't need to worry too much about the
       number of blanks or characters because the order they appear is fixed,
       but because of the * wildcard, we have to track where we are in the
       input pattern. */

    /* The support for the * wildcard is absolutely brutal, mainly due to my
       wanting to work it into the original algorithm.  It's not as fast as it
       could be (for many reasons) and has a very "unclean" feel to it.
       Nevertheless it seems to work, and that's what counts.  I've tried to
       document it as best I can. */

    /* The recursion-mimicking part of this algorithm is taken from the
       anagramming algorithms below. */

    /* TODO:  Rewrite this, ideally with a finite state machine to allow proper
              regular expression searches. */

    for (; count < NODESPERSEARCH; count++)
    {
        match = false;
        if (!onstar[searchpos])
        {
            /* Not on a star so just try to match like normal. */

            for (i = 0; i < MAXSPECIAL; i++)
            {
                if (searchword[searchpos] == specialChar[i] &&
                    IsSpecialChar(searchnode->letter, i))
                {
                    match = true;
                    wcmatchpos[indent] = false;
                    if (searchword[searchpos] == CHAR_BLANK)
                        wcmatchpos[indent] = true;
                    break;
                }
            }
            if (!match && (searchnode->letter == searchword[searchpos]))
            {
                match = true;
                wcmatchpos[indent] = false;
            }
        }
        else
        {
            /* We are on a star, and the only way out (other than hitting the
               end of the pattern) is to match the character AFTER the star, so
               we have to check that first. */

            searchpos++;
            if (searchpos < searchwordlen)
            {
                for (i = 0; i < MAXSPECIAL; i++)
                {
                    if (searchword[searchpos] == specialChar[i] &&
                        IsSpecialChar(searchnode->letter, i))
                    {
                        match = true;
                        wcmatchpos[indent] = false;
                        if (searchword[searchpos] == CHAR_BLANK)
                            wcmatchpos[indent] = true;
                        break;
                    }
                }
                if (!match && (searchnode->letter == searchword[searchpos]))
                {
                    match = true;
                    wcmatchpos[indent] = false;
                }
            }

            /* If we didn't match the next character then we must be
               matching via the *.  Everything matches a *, of course. */
            if (!match)
            {
                match = true;
                searchpos--;
                wcmatchpos[indent] = true;
            }
        }
        if (!match)  /* onstar must be false if this is the case. */
        {
            /* Backup to the previous * if there is one and try
               to match again.  If there isn't one, we can't possibly match
               anymore, so we'll fall through to the bottom of the loop
               where we'll move on to the next node. */

            /* It's tricky to explain why we back up, so here's an example.
               Suppose the pattern is *INK and the current word is INTERLINK.
               After the first IN, we'll try to match the K and fail.  But we
               might match later, so we have to return to the * and hope.  In
               effect, by returning to the * we change the IN from the matching
               the literal to matching the *, which frees up the literal for
               later use. */

            if (prevstar[searchpos] >= 0)
            {
                /* The ugliest part of the * support is keeping the right
                   position in the input pattern when moving about the graph.
                   As with the current node, we want to save the position at
                   each level.  However, we want to also make sure that nodes
                   in the subgraph have the chance to match, so we need to keep
                   track of the "ideal" position to match, which turns out to
                   be the highest position in the input encountered along the
                   current path.  Then later when we back up from deeper into
                   the search we can start where we left off originally.  This
                   all depends on the fact that we back up to the last star if
                   things don't work out later on. */

                if (searchpos > searchmatchpos[indent])
                    searchmatchpos[indent] = searchpos;
                
                for (i = 0; i < searchpos - prevstar[searchpos]; i++)
                    wcmatchpos[indent - i] = true;
                    
                searchpos = prevstar[searchpos];
                continue;
            }
        }

        if (match)
        {
            searchbuf[indent] = searchnode->letter;
            /* See if we're in a position to match a word.  We either have
               to consume the whole input, or everything but the last star
               (because a * can stand for 0 characters).  Note that we never
               have ** so we only have to worry about one. */
            if (searchpos == searchwordlen - 1 ||
                (laststar == searchwordlen - 1 &&
                 searchpos == laststar - 1))
            {
                /* See if a word does indeed end here and if it's in the
                   right category. */
                if (searchnode->endofword &&
                    dbhdrptr->catinclude[state.category][searchnode->category])
                {
                    /* The word can be added. */
                    /* I think at one time AddWordToListDB() used indent
                       to determine the length, but this is no longer the
                       case.  Nevertheless I've left the temporary
                       increase-then-decrease in place. */
                    searchbuf[++indent] = '\0';
                    if (!AddWordToListDB(searchnode->category))
                    {
                        searchfull = true;
                        return;
                    }
                    --indent;
                }
            }
            /* We can always go further down if a * is present. */
            if (searchpos < searchwordlen - 1 || numstars > 0)
            {
                /* It's important to save the position even if we don't go
                   down.  If we didn't, we could end up with a value that is
                   too low so when future letters at this level are tested they
                   have no hope of matching even when they should.  (I know
                   this because I was missing this piece of the puzzle for a
                   long time.) */
                if (searchpos > searchmatchpos[indent])
                    searchmatchpos[indent] = searchpos;

                /* If we can go deeper, do so. */
                if (searchnode->child)
                {
                    /* Save the state for this level.  This is what
                       mimics the recursion. */
                    stemnodes[indent] = searchnode;
                    stemrecs[indent] = searchrec;

                    indent++;
                    /* Note that searchpos can go past the end of the input to
                       the terminating '\0'.  However, if it does then nothing
                       will match and the search will return to the previous *.
                       This is why we keep prevstar[] for MAXINPUTLENTH + 1
                       positions. */
                    searchpos++;

                    /* It's also important to save the current position for the
                       NEXT level, because if we don't and the first node on
                       doesn't match we won't save it, and we'll go back to the
                       previous star, which means again some legitimate matches
                       have no hope.  (Again, I know this because I was missing
                       it for a long time.) */
                    if (searchpos > searchmatchpos[indent])
                        searchmatchpos[indent] = searchpos;

                    searchrec = searchnode->child / DBRECORDLENGTH;
                    searchnode = dbnodeptr[searchrec] +
                           (searchnode->child % DBRECORDLENGTH);
                    /* We don't want to skip the first node in the
                       next level! */
                    continue;
                }
            }
        }

        /* End of the level - back up as far as needed to get to a fresh
           node. */
        while (searchnode->endoflist)
        {
            /* If we're at the end of the first level, we're done! */
            if (indent == 0)
            {
                searching = false;
                return;
            }

            /* Go back to parent. */
            searchmatchpos[indent] = 0;
            indent--;
            searchnode = stemnodes[indent];
            searchrec = stemrecs[indent];
            searchpos = searchmatchpos[indent];
        }

        /* Go to the next node. */
        searchnode++;
        searchpos = searchmatchpos[indent];
        /* Because the nodes are stored across several records, we need to
           check if we're at the end of one record and move to the first node
           of the next record if we are. */
        if ((UInt8*) searchnode >= (dbbyteptr[searchrec] +
                                    MemPtrSize(dbbyteptr[searchrec])))
        {
            searchrec++;
            ErrFatalDisplayIf(searchrec == numrecs,
                              "Search moved past last record.");
            searchnode = dbnodeptr[searchrec];
        }
    }

}

/*****************************************************************************/

/* SearchAnagram() - Performs a search in Anagram (ANA) mode.
   Parameters:  None.
   Returns:  Nothing.
   Notes:  The anagram search matches the input in any order. */
static void SearchAnagram(void)
{
    UInt16 count = 0;        /* Counts the number of iterations. */
    Boolean addedchar;       /* Did we match a character this time? */
    Boolean foundword;       /* Did we find a word this time? */
    Int8 i,j;                /* wild card counters */

    /* The algorithm used here is based on the algorithm in the publically
       available dawg_words.c by John J. Chew III (jjchew@math.utoronto.ca).
       I've cleaned it up a bit (the original was rather perl-ish) and modified
       it slightly to better suit LAMPWords and the Palm devices. */

    /* The anagramming algorithm is similar to the pattern algorithm above
       except characters can match in any order.  To accomodate this we
       precount the number of characters and blanks, then instead of matching a
       character directly we just look to see if there's one available and
       decrement its count if there is.  If not and we have a blank left, we'll
       use it.  To avoid duplication we always and only take the actual
       character if it's available instead of a blank.  (If we took the blank
       we'd miss words, and if we tried both we'd duplicate words.) */

    /* Aside from the above differences the algorithm isn't too much different
       than the pattern algorithm above, so only the differences will be
       noted. */

    /* When a * is used every character "counts" and the determining factor
       when end-of-word is detected is if all of the other specified characters
       have been seen.  To check this we count whenever we do a non-star match
       and check this count against the (predetermined) number of non-star
       characters in the input.  If they're equal, we've seen everything we
       need and can add the word.  Notice that one * is sufficient, which is
       why the input processor strips off more than one in this mode. */

    /* NOTE:  My gut tells me that the ZeroCounts() function is unnecessary if
              the special character flags are managed carefully enough.  If
              these calls could be removed it would speed things up a bit. */

    for (; count < NODESPERSEARCH; count++)
    {
        addedchar = false;
        foundword = false;

        /* See if we're matching via a real character... */
        if (numchars[(UInt8) searchnode->letter] > 0)
        {
            numchars[(UInt8) searchnode->letter]--;
            ZeroCounts(indent);
            addedchar = true;
            numnonstarmatches++;
        }
        else /* ...or a special character. */
        {
            /* we have to iterate backwards so that the less composite types
               are done before the combined types (in order of increasing
               size).  (JLD) */
            for (j = MAXSPECIAL - 1; j >= 0; j--)
            {
                if (numSpecial[j] > 0 && IsSpecialChar(searchnode->letter, j))
                {
                    numSpecial[j]--;
                    ZeroCounts(indent);
                    isSpecial[j][indent] = 1;
                    addedchar = true;
                    numnonstarmatches++;
                    if (j == 0) // Match via blank
                        wcmatchpos[indent] = true;        
                    break;
                }
            }
        }

        /* If we didn't match above and we have a * then we match now, but of
           course we don't remove the character from any pool.  Also we have
           to note this fact so we don't try to add it back to the pool on the
           way back up the graph traversal. */
        if (!addedchar && numstars > 0)
        {
            starmatch[indent] = true;
            addedchar = true;
            wcmatchpos[indent] = true;
        }

        /* If we did indeed match... (always true if a star is present) */
        if (addedchar)
        {
            searchbuf[indent] = searchnode->letter;
            if (indent + 1 == searchwordlen || numstars > 0)
            {
                if (searchnode->endofword &&
                    dbhdrptr->catinclude[state.category][searchnode->category])
                {
                    if (numstars == 0 || numnonstarmatches == numnonstars)
                    {
                        searchbuf[++indent] = '\0';
                        if (!AddWordToListDB(searchnode->category))
                        {
                            searchfull = true;
                            return;
                        }
                        --indent;
                        foundword = true;
                    }
                }
            }
            if (indent + 1 < searchwordlen || numstars > 0)
            {
                if (searchnode->child)
                {
                    stemnodes[indent] = searchnode;
                    stemrecs[indent] = searchrec;
                    indent++;
                    searchrec = searchnode->child / DBRECORDLENGTH;
                    searchnode = dbnodeptr[searchrec] +
                           (searchnode->child % DBRECORDLENGTH);
                    continue;
                }
            }

            /* We can't go further down so just return this character to
               the pool and carry on. */
            if (!starmatch[indent])
            {
                for (i=0; i<MAXSPECIAL; i++)
                {
                    if (isSpecial[i][indent] > 0)
                    {
                        numSpecial[i]++;
                        wcmatchpos[indent] = false;
                        break;
                    }
                }
                if (i == MAXSPECIAL)
                {
                    numchars[(UInt8) searchnode->letter]++;
                }
                numnonstarmatches--;
            }
            else
            {
                starmatch[indent] = false;
                wcmatchpos[indent] = false;
            }
        }

        while (searchnode->endoflist)
        {
            if (indent == 0)
            {
                searching = false;
                return;
            }

            indent--;

            /* Return characters to the pool as we move back up.  Naturally,
               only characters that we took out (i.e. not matched by a *)
               should be returned. */
            if (!starmatch[indent])
            {
                for (i=0; i<MAXSPECIAL; i++)
                {
                    if (isSpecial[i][indent] > 0)
                    {
                        numSpecial[i]++;
                        wcmatchpos[indent] = false;
                        break;
                    }
                }
                if (i == MAXSPECIAL)
                {
                    numchars[(UInt8) searchbuf[indent]]++;
                }
                numnonstarmatches--;
            }
            else
            {
                starmatch[indent] = false;
                wcmatchpos[indent] = false;
            }
            searchnode = stemnodes[indent];
            searchrec = stemrecs[indent];
        }
        searchnode++;
        if ((UInt8*) searchnode >=
            (dbbyteptr[searchrec] + MemPtrSize(dbbyteptr[searchrec])))
        {
            searchrec++;
            ErrFatalDisplayIf(searchrec == numrecs,
                              "Search moved past last record.");
            searchnode = dbnodeptr[searchrec];
        }
    }

}

/*****************************************************************************/

/* SearchBuild() - Performs a search in Build (BLD) mode and Fit (FIT) Mode.
   Parameters:  None.
   Returns:  Nothing.
   Notes:  The build search matches the input in any order, but unlike
           anagramming, Build mode matches words of any length.  If invoked in
           Fit mode, a pattern will be applied to further filter the results -
           this means all words will have the same length. If the global
           variable searchpatlen is 0, Build mode is used, otherwise Fit mode
           is used. */
static void SearchBuild(void)
{
    UInt16 count = 0;        /* Counts the number of iterations. */
    Boolean addedchar;       /* Did we match a character this time? */
    Int8 i,j;                /* wild card counters */

    /* The build algorithm is exactly like the anagramming algorithm above
       except we match words of any length.  This means for any letter we
       match, if the endofword flag is set we add the word, regardless of the
       length.  It also means that even if we match we still try to go down to
       the next level because we might not have covered all the characters
       yet.  Other than that this algorithm is identical to the anagramming
       algorithm above. */

    /* When in Fit mode, words are checked against a separate pattern before
       being considered a match.  This means matching words will all be the
       same length, and we can take advantage of this fact to speed up the
       search a bit.  NOTE:  The reason we use Build and not Anagram for Fit
       mode is so patterns can be matched from a pool of more letters than is
       needed to fit the pattern. */

    /* * has no use in Build mode; it would match every word in the dictionary
       by definition.  Using MAXINPUTLENGTH blanks is equivalent, though
       practically it would make no sense.  In Fit mode it makes even less
       search because then a plain Pattern search would be equivalent; but much
       faster! */

    for (; count < NODESPERSEARCH; count++)
    {
        addedchar = false;

        if (numchars[(UInt8) searchnode->letter] > 0)
        {
            numchars[(UInt8) searchnode->letter]--;
            ZeroCounts(indent);
            addedchar = true;
        }
        else
        {
            for (j = MAXSPECIAL - 1; j >= 0; j--)
            {
                if (numSpecial[j] >0 && IsSpecialChar(searchnode->letter, j))
                {
                    numSpecial[j]--;
                    ZeroCounts(indent);
                    isSpecial[j][indent] = 1;
                    addedchar = true;
                    if (j == 0) // Match via blank
                        wcmatchpos[indent] = true;        
                    break;
                }
            }
        }

        if (addedchar)
        {
            searchbuf[indent] = searchnode->letter;
            if (searchnode->endofword &&
                dbhdrptr->catinclude[state.category][searchnode->category])
            {
                searchbuf[++indent] = '\0';
                if (searchpatlen == 0 || FitPattern(false, searchpat, searchbuf))
                {
                    if (!AddWordToListDB(searchnode->category))
                    {
                        searchfull = true;
                        return;
                    }
                }
                --indent;
            }
            
            if (searchnode->child)
            {
                stemnodes[indent] = searchnode;
                stemrecs[indent] = searchrec;
                indent++;
                searchrec = searchnode->child / DBRECORDLENGTH;
                searchnode = dbnodeptr[searchrec] +
                       (searchnode->child % DBRECORDLENGTH);
                continue;
            }

            for (i=0; i<MAXSPECIAL; i++)
            {
                if (isSpecial[i][indent] > 0)
                {
                    numSpecial[i]++;
                    wcmatchpos[indent] = false;
                    break;
                }
            }
            if (i == MAXSPECIAL)
            {
                numchars[(UInt8) searchnode->letter]++;
            }
            
        }
        while (searchnode->endoflist)
        {
            if (indent == 0)
            {
                searching = false;
                return;
            }
            indent--;

            for (i=0; i<MAXSPECIAL; i++)
            {
                if (isSpecial[i][indent] > 0)
                {
                    numSpecial[i]++;
                    wcmatchpos[indent] = false;
                    break;
                }
            }
            if (i == MAXSPECIAL)
            {
                numchars[(UInt8) searchbuf[indent]]++;
            }

            searchnode = stemnodes[indent];
            searchrec = stemrecs[indent];
        }
        searchnode++;
        if ((UInt8*) searchnode >=
            (dbbyteptr[searchrec] + MemPtrSize(dbbyteptr[searchrec])))
        {
            searchrec++;
            ErrFatalDisplayIf(searchrec == numrecs,
                              "Search moved past last record.");
            searchnode = dbnodeptr[searchrec];
        }
    }

}

/*****************************************************************************/
/* Search Helper Functions.                                                  */
/*****************************************************************************/

/*****************************************************************************/
/* ZeroCounts() - Initializes wild card counts for ANA and BLD modes.  (JLD)
   Parameters:  None.
   Returns:  Nothing.*/
static void ZeroCounts(UInt8 index)
{
    UInt8 i = 0;
 
    for (i=0; i<MAXSPECIAL; i++)
    {
        isSpecial[i][index] = 0;
    }
}

/*****************************************************************************/

/* FitPattern() - Checks if a searched word matches the search pattern.
   Parameters:  usingStar - Have we encountered a * yet?
   				searchPattern - Remainder of pattern to check.
   				searchBuffer - Remainder of word buffer to check.
   Returns:  true if the word matches, otherwise false.
   Notes:  Parameters are obtained from the global search variables.  This
           function is called by SearchBuild() and is used only in Fit mode.
           This function is recursive.  After matching up to a * it will
           call itself to match the rest of the pattern. */
static Boolean FitPattern(Boolean usingStar, Char* searchPattern, Char* searchBuffer)
{
	/* As we can only do (MAXINPUTLENGTH + 1)/2 recursions we can get away
	   with using all these locals without running out of stack space. */
    UInt8 i;       /* Loop counter. */
    UInt8 j;       /* Loop counter. */
    Char pattern[MAXINPUTLENGTH + 1]; /* Local copy of search pattern. */
    Char* p;  /* Pointer to current pattern. */
    Char* r;  /* Pointer to remaining pattern. */
    Char* b;  /* Pointer to remaining buffer. */
    Int8 len;    /* Length of string at p (stored for speed). */
    Int8 bufLen; /* Length of string at b (stored for speed). */
    Boolean match;     /* Did we match via special character? */
    
    /* We use a local copy of the search pattern because we will modify it
       as we go to save time. */
	MemSet(pattern, MAXINPUTLENGTH + 1, 0);
	StrCopy(pattern, searchPattern);

	/* This is basically a recursive version of SearchPattern() that works
	   on a regular string buffer rather than the word database.  This
	   algorithm works by effectively splitting the pattern into sections
	   with the * as the delimiter, then all we have to do is check to make
	   sure each subsection of the pattern occurs at some point of the
	   word (in order). */
	   
	/* The recursion is necessitated by the possibility that we may match
	   everything "too soon" and wind up at the end of the pattern before
	   we are at the end of the word.  For example, consider the word RINGING
	   and the pattern R*ING.  We will match the R and the first ING, but
	   not be at the end of the word.  So we have to "back up" to the *
	   and continue, starting from the next letter (the first N).  On top of
	   that, this could happen several times with several *s, and we don't
	   know how far we have to "back up" to match.  Recursion lets us do this
	   without having to keep track of all the star locations and stuff like
	   we do in SearchPattern(). */

	r = pattern;
	b = searchBuffer;
	bufLen = StrLen(b);
	while (r != NULL)
	{
		p = r;	
		r = StrChr(r, CHAR_STAR);
		if (r != NULL)
		{
			/* The location of the next * becomes the end point for this
			   section of the pattern. */
			*r = '\0';
			r++; /* r now points to first char in remainder of pattern. */
		}
		len = StrLen(p);
		
		/* Loop through the buffer (search word) and see if we can match the
		   pattern starting at any character. */
		while (1)
		{
			/* If we run out of room to match this section we can never match
			   the word. */
			if (bufLen < len)
				return false;

			/* This loop does the actual compare. */				
			for (i = 0; i < len; i++)
			{
				match = false;
				
	            for (j = 0; j < MAXSPECIAL; j++)
	            {
	                if (p[i] == specialChar[j] && IsSpecialChar(b[i], j))
	                {
	                	match = true;
	                	break;
	                }
	            }
	            
				if (!match && p[i] != b[i])
					break;
			}
			if (i == len)
			{
				/* If our next * happens to be the end of the string, we
				   have matched all the sections and everything else will
				   match, so we're done! */
				if (r != NULL && *r == '\0')
					return true;
				if (r == NULL)
				{
					/* No more stars.  If we are at the end of the word,
					   we match! */
					if (*(b + i) == '\0')
						return true;
				}
				else
				{
					/* If the recursive search fails we simply continue the
					   loop. */
					if (FitPattern(true, r, b + i))
						return true;
				}
			}
			/* If we have not yet seen a *, we must match starting at the first
			   position. */
			if (!usingStar)
				return false;
			/* Consume next buffer character. */
			b++;
			bufLen--;
		}
		usingStar = true;
	}

	/* We are done with the entire pattern, so the last check we make is to
	   be sure the word actually ends here too. */
//    return (*b == '\0');
	return false;
}

/*****************************************************************************/
/* Word Judging Function.                                                    */
/*****************************************************************************/

/* WordJudge() - Checks for presence of word(s) in the word database.
   Parameters:  None
   Returns:  true if all words are in the database, false if at least one
             word is not.
   Notes:  Used in conjunction with the word judge form.  This function
           essentially performs a pattern search on all of the words entered
           into the word judge form.  It doesn't need to store the results,
           rather it only needs to check if each word is in the database (and
           matches the selected category).
           Thus this function is a stripped-down version of the pattern
           search function which is _not_ passthrough like the other search
           functions (since pattern lookups with no wildcards are very fast)
           and also does its own initialization and termination.  We do
           borrow some of the global variables from the normal search, though.
           This function follows the word judging rules for tournament
           Scrabble(r) in North America.  The user is only told if the _play_
           (i.e. all challenged words) is acceptable but not the status of each
           word. */
static Boolean WordJudge(void)
{
    UInt8 d;                      /* Loop counter. */
    /* Copies of the filtered words, which will be shown to the user
       with the ruling to ensure the words were judged as intended. */
    Boolean acceptable = true;    /* Return value. */
    Boolean notfound;             /* Did we fail to find the current word? */

    /* We run the algorithm on all non-empty (after filtering out invalid
       characters) fields. */

    MemSet(wjwords, NUMWJWORDS * (MAXINPUTLENGTH + 1), 0);
    for (d = 0; d < NUMWJWORDS; d++)
    {
        textptr = FldGetTextPtr(wjwordfield[d]);
        if (textptr != NULL)
        {
            /* Initialization. */
            notfound = false;
            StrCopy(wjwords[d], textptr);
            CleanUpStr(wjwords[d], true);

            /* Make sure we have something to judge. */
            if (!wjwords[d][0])
                continue;

            StrCopy(searchword, wjwords[d]);
            ConvStr(searchword);
            searchwordlen = StrLen(searchword);
            indent = 0;

            searchnode = dbstartnode;
            searchrec = 0;

            while (1)
            {
                /* Match the character only - no blanks allowed. */
                if (searchnode->letter == searchword[indent])
                {
                    /* See if we're as far down as we can go. */
                    if (indent + 1 == searchwordlen)
                    {
                        /* See if a word does indeed end here and if it's in
                           the right category. */
                        if (searchnode->endofword &&
                            dbhdrptr->catinclude[state.category]
                                                [searchnode->category])
                        {
                            /* Nothing to do if we find the word, so we
                               can move to the next one. */
                            break;
                        }
                    }
                    else   /* We might be able to go further down. */
                    {
                        /* If we can go deeper, do so. */
                        if (searchnode->child)
                        {
                            /* Save the state for this level.  This is what
                               mimics the recursion. */
                            stemnodes[indent] = searchnode;
                            stemrecs[indent] = searchrec;

                            indent++;
                            searchrec = searchnode->child / DBRECORDLENGTH;
                            searchnode = dbnodeptr[searchrec] +
                                   (searchnode->child % DBRECORDLENGTH);
                            /* We don't want to skip the first node in the
                               next level! */
                            continue;
                        }
                    }
                }

                /* End of the level - back up as far as needed to get to a
                   fresh node. */
                while (searchnode->endoflist)
                {
                    /* If we're at the end of the first level, we're done! */
                    if (indent == 0)
                    {
                        notfound = true;
                        break;
                    }

                    /* Go back to parent. */
                    indent--;
                    searchnode = stemnodes[indent];
                    searchrec = stemrecs[indent];
                }

                if (notfound)
                {
                    /* Even if the first word is found to be invalid we'll
                       continue on and "judge" the rest, because if we didn't
                       the user might be able to gauge how long it takes to
                       judge a single word and use that knowledge to determine
                       that the first word was the unacceptable one, and we're
                       trying not to let the user know which word(s) was
                       invalid. */
                    acceptable = false;
                    break;        /* Go on to the next word. */
                }

                /* Go to the next node. */
                searchnode++;
                /* Because the nodes are stored across several records, we need
                   to check if we're at the end of one record and move to the
                   first node of the next record if we are. */
                if ((UInt8*) searchnode >= (dbbyteptr[searchrec] +
                                            MemPtrSize(dbbyteptr[searchrec])))
                {
                    searchrec++;
                    ErrFatalDisplayIf(searchrec == numrecs,
                                      "Word judge moved past last record.");
                    searchnode = dbnodeptr[searchrec];
                }
            }
        }
    }

    return acceptable;
}

/*****************************************************************************/
/* Beaming Functions.                                                        */
/*****************************************************************************/

/* NOTE:  These functions were taken directly from the Palm OS Programmer's
   Companion, Volume II, pages 27-28, listing 1.8. */

/* Callback for ExgDBWrite to send data with Exchange Manager */
Err WriteDBData(const void* dataP, UInt32* sizeP, void* userDataP)
{
    Err err;

    *sizeP = ExgSend((ExgSocketPtr)userDataP, (void*)dataP, *sizeP, &err);

    return err;
}

/* Sends any database in the handheld. */
Err SendDatabase(UInt16 cardNo, LocalID dbID, Char* nameP,
                 Char* descriptionP)
{
    ExgSocketType exgSocket;
    Err err;

    /* Create exgSocket structure */
    MemSet(&exgSocket, sizeof(exgSocket), 0);
    exgSocket.description = descriptionP;
    exgSocket.name = nameP;
    /* Start an exchange put operation */
    err = ExgPut(&exgSocket);
    if (!err)
    {
        err = ExgDBWrite(WriteDBData, &exgSocket, NULL, dbID, cardNo);
        err = ExgDisconnect(&exgSocket, err);
    }

    return err;
}

/* Sends the LAMPWordsDB database.  (App launcher will receive & store it.) */
Err SendMe(void)
{
    Err err;

    /* We already know the dictionary's LocalID from startup. */
    if (lwdbid)
        err = SendDatabase(lwdbcard, lwdbid, "LAMPWordsDB.pdb",
                           "LAMPWords Dictionary");
    else
        err = DmGetLastErr();

    return err;
}

/*****************************************************************************/
/* Palm OS-related Functions.                                                */
/*****************************************************************************/

/* StartApplication() - Performs initialization when the program is started.
   Parameters:  None.
   Returns:  1 if something fails during initialization, 0 on success. */
static Err StartApplication(void)
{
    NumberFormatType numFormat;   /* Information on the number format. */
    UInt16 size;                  /* Size of various data structures. */
    Int16 res;                    /* Result code. */
    UInt16 d;                     /* Loop counter. */
    UInt32 romver;                /* Palm OS ROM version number. */

    /* Determine the Palm OS version.  This code is taken from the
       Palm reference manual.  If we're on 3.1 or greater we can use the
       Progress Dialogs. */
    FtrGet(sysFtrCreator, sysFtrNumROMVersion, &romver);
    if (romver >= sysMakeROMVersion(3, 1, 0, sysROMStageRelease, 0))
    {
        useprog = true;
    }
    /* If we're on 4.0 or greater we can call LstGetTopItem().  If not,
       we'll pull the top item directly out of the list object data structure
       (something that's generally not recommended).  Note that I could have
       used the PalmOSGlue library to handle this for me, but decided it wasn't
       worth linking in a bunch of extra code just to perform one cosmetic
       function. */
    if (romver >= sysMakeROMVersion(4, 0, 0, sysROMStageRelease, 0))
    {
        usegettop = true;
    }

    /* Retrieve information about the last running state from the OS's
       preferences database, to make LAMPWords look as it did when the user
       last quit the program. */
    size = sizeof(LWState);
    res = PrefGetAppPreferences(CREATORID, LWSTATEID,
                                &state, &size, false);
    if (res != LWSTATEVERSION || size != sizeof(LWState))
    {
        /* Assign defaults if we don't find any of if the version is
           wrong. */
        MemSet(&state, sizeof(LWState), 0);
        state.searchmode = modePattern;
    }

    /* Retrieve configuration information (preferences) as stored in the
       OS preference database. */
    size = sizeof(LWPrefs);
    res = PrefGetAppPreferences(CREATORID, LWPREFSID,
                                &prefs, &size, true);
    if (res != LWPREFSVERSION || size != sizeof(LWPrefs))
    {
        /* Assign defaults if we don't find any of if the version is
           wrong. */
        MemSet(&prefs, sizeof(LWPrefs), 0);
        prefs.listrecs = 2;
        prefs.preservelist = true;
        prefs.showAlphagram = false;
        prefs.showSymbols = true;
        prefs.highlightMatches = false;
    }

    /* Open the word database, trying each card in turn.  We'll keep it open
       throughout the life of the program. */
    for (d = 0; d <= MAXCARDS; d++)
    {
        lwdbid = DmFindDatabase(d, "LAMPWordsDB");
        if (lwdbid)
        {
            lwdb = DmOpenDatabase(d, lwdbid, dmModeReadOnly);
            lwdbcard = d;
            break;
        }
    }
    if (!lwdbid)
    {
        FrmAlert(DBErrorAlert);
        return 1;
    }

    /* We'll lock the memory for the word database and keep it locked for the
       duration of the program.  Since the word database is read-only from
       LAMPWords' point of view we may as well do this now and then we don't
       waste time doing it for each search. */

    /* First, get the header record. */
    dbhdr = DmQueryRecord(lwdb, 0);
    dbhdrptr = (DBHeader*) MemHandleLock(dbhdr);
    ErrFatalDisplayIf(dbhdrptr == NULL, "Bad Lock on Hdr");
    if (dbhdrptr == NULL)
    {
        FrmAlert(DBErrorAlert);
        return 1;
    }

    /* See if this is a valid word database and if it's the right version. */
    if (StrCompare(dbhdrptr->magic, DBMAGICSTRING))
    {
        FrmAlert(DBOldAlert);
        return 1;
    }

    /* See if it's the same database we last used.  If not we won't try to
       show any old results since there may be inconsistencies with the
       categories, character set, etc. */
    if (StrCompare(dbhdrptr->name, state.dictname))
    {
        state.wordcount = 0;
        state.topitem = 0;
        StrCopy(state.dictname, dbhdrptr->name);
        state.category = 0;
    }

    /* We start by creating arrays for the record IDs and pointers.  Note that
       the zeroth record is the header record which we already got above. */
    numrecs = DmNumRecords(lwdb) - 1;
    dbrec = MemPtrNew(sizeof(MemHandle*) * numrecs);
    dbnodeptr = MemPtrNew(sizeof(DBNode*) * numrecs);
    dbbyteptr = MemPtrNew(sizeof(Char*) * numrecs);
    if (!dbrec || !dbnodeptr || !dbbyteptr)
    {
        FrmAlert(OutOfMemAlert);
        return 1;
    }

    /* Grab all the records. */
    for (d = 0; d < numrecs; d++)
    {
        /* Again we're ignoring the 0th record.  To make the math easier
           (and thus faster) during searching we'll store record 1 in position
           0, record 2 at pos. 1, etc. */
        dbrec[d] = DmQueryRecord(lwdb, d + 1);
        dbnodeptr[d] = (DBNode*) MemHandleLock(dbrec[d]);
        ErrFatalDisplayIf(dbnodeptr[d] == NULL, "Bad Lock");
        if (dbnodeptr[d] == NULL)
        {
            FrmAlert(DBErrorAlert);
            return 1;
        }
        dbbyteptr[d] = (UInt8*) dbnodeptr[d];
    }

    /* Map the category names to a list of pointers so they can be used
       by LstSetListChoices(). */
    for (d = 0; d < dbhdrptr->numcategories; d++)
    {
        catnames[d] = dbhdrptr->catname[d];
    }

    /* A safeguard in case the dictionary categories have changed. */
    if (state.category >= dbhdrptr->numcategories)
    {
        state.category = 0;
    }

    /* TODO:  Should do some record size checking and stuff for safety. */

    /* Handy ID for the starting node.  Before switching to the current
       format I was using a format where the search started at the end of
       the database, so this used to be more useful. */
    dbstartnode = dbnodeptr[0] + 1;

    /* Clear the search result list database variables.  We'll open this and
       get any old results later when the main form is created. */
    for (d = 0; d < MAXLISTDBRECS; d++)
    {
        listdbrecmem[d] = 0;
        listdbrecptr[d] = NULL;
    }

    /* Get the number format from the OS settings. */
    numFormat = (NumberFormatType) PrefGetPreference(prefNumberFormat);
    LocGetNumberSeparators(numFormat, &thsep, &decsep);

    /* Buffer the strings stored in the program's resources. */
    for (d = 0; d <= NUMMODES; d++)
    {
        SysStringByIndex(ModeAdjST, d, modeadj[d], 21);
    }
    SysCopyStringResource(histstr, InpHistStr);
    SysStringByIndex(ProgTextST, 0, progtext[0], 21);
    SysStringByIndex(ProgTextST, 1, progtext[1], 21);
    SysStringByIndex(RulingST, 0, rulingtxt[0], 21);
    SysStringByIndex(RulingST, 1, rulingtxt[1], 21);
    SysCopyStringResource(version, VersionStr);
    SysStringByIndex(FitTextST, 0, fittext[0], 21);
    SysStringByIndex(FitTextST, 1, fittext[1], 21);

    /* All done, let's go! */

    FrmGotoForm(LWForm);
	return 0;
}

/*****************************************************************************/

/* StopApplication() - Performs deinitialization when the program ends.
   Parameters:  None.
   Returns:  Nothing. */
static void StopApplication(void)
{
    Char* ptr;     /* Pointer to text field memory. */
    UInt8 d;       /* Loop counter. */

    /* Store the program's current state (appearance). */
    state.searchmode = GetSearchMode();
    ptr = (Char*) FldGetTextPtr(inputfield);
    if (ptr[0])
        StrCopy(state.inputfield, ptr);
    else
        StrCopy(state.inputfield, state.inputhistory[0]);
    state.topitem = usegettop ? LstGetTopItem(wordlist) : wordlist->topItem;

    /* Store the state, as well as the configured settings. */
    PrefSetAppPreferences(CREATORID, LWSTATEID, LWSTATEVERSION,
                          &state, sizeof(LWState), false);
    PrefSetAppPreferences(CREATORID, LWPREFSID, LWPREFSVERSION,
                          &prefs, sizeof(LWPrefs), true);

	FrmCloseAllForms();

    /* Unlock memory and close the search results database. */
    if (listdb != 0)
    {
        for (d = 0; d < MAXLISTDBRECS; d++)
        {
            if (listdbrecmem[d] != 0)
            {
                MemHandleUnlock(listdbrecmem[d]);
                DmReleaseRecord(listdb, d, true);
            }
        }
        DmCloseDatabase(listdb);

        /* Unless the user wants otherwise we won't delete the search
           results before exiting.  This means we can show them next time
           the program runs, which can be handy if the user accidentally hit a
           button or just had to run something else after completing a long
           search.  However the results can take up a lot of memory so they
           can be deleted if needed. */
        if (!prefs.preservelist)
        {
            DmDeleteDatabase(listdbcard, listdbid);
        }
    }

    /* Unlock and close the word database. */
    for (d = 0; d < numrecs; d++)
    {
        MemHandleUnlock(dbrec[d]);
    }
    MemHandleUnlock(dbhdr);
    MemPtrFree(dbbyteptr);
    MemPtrFree(dbnodeptr);
    MemPtrFree(dbrec);
    DmCloseDatabase(lwdb);

}

/*****************************************************************************/

/* LWFormEventHandler() - Handles events for the main form.
   Parameters:  event - Pointer to the event's data.
   Returns:  true if we handled the event, false if we didn't. */
Boolean LWFormEventHandler(EventPtr event)
{
    Boolean handled = false; /* Did we handle the event? */
    WinDirectionType dir;    /* Scroll direction. */
    Int16 val;               /* Repeat value. */
    Int8 d;                  /* Loop counter. */
    Int16 res;               /* Result code. */
    DmSearchStateType ds;    /* Info. for list database. */

    switch (event->eType)
    {
    /* Open the form. */
    case frmOpenEvent:
        curform = FrmGetActiveForm();
        FrmDrawForm(curform);
        FrmSetFocus(curform, FrmGetObjectIndex(curform, InputField));

        /* Initialize controls. */
        SetFieldText(countfield, "0", COUNTFIELDMEMSIZE + 1);
        SetFieldText(inputfield, state.inputfield, MAXINPUTLENGTH + 1);
        FldSetSelection(inputfield, 0, StrLen(state.inputfield));
        if (state.searchmode == modeFit)
            CtlSetValue(fitpush, 1);
        else if (state.searchmode == modeBuild)
            CtlSetValue(bldpush, 1);
        else if (state.searchmode == modeAnagram)
            CtlSetValue(anapush, 1);
        else
            CtlSetValue(patpush, 1);
        LstSetListChoices(catlist, catnames, dbhdrptr->numcategories);
        LstSetSelection(catlist, state.category);
        CtlSetLabel(cattrig, catnames[state.category]);
        LstSetDrawFunction(wordlist, DrawWord);
        for (d = 0; d < HISTORYSIZE; d++)
        {
            histlistitems[d] = state.inputhistory[d];
        }
        LstSetListChoices(histlist, histlistitems, HISTORYSIZE);
        CtlSetLabel(histtrig, histstr);

        /* Look for an existing search results database.  If we find one
           we'll put these results in the list so the program looks as
           much like it did when it was exited as possible. */
        res = DmGetNextDatabaseByTypeCreator(true, &ds, LISTDBID,
                                             LISTDBCREATORID, false,
                                             &listdbcard, &listdbid);
        if (res != errNone)
        {
            state.wordcount = 0;
        }

        /* If a potential incompatibility with the existing results was
           detected during initialization, state.wordcount will be set to 0
           to tell us not to try to restore the old results here. */
        if (state.wordcount > 0)
        {
            listdb = DmOpenDatabase(listdbcard, listdbid, dmModeReadOnly);
            if (listdb != NULL)
            {
                res = DmNumRecords(listdb);
                /* Check to make sure the word count is suitable for the
                   database size.  If not, something probably went wrong
                   somewhere.  Most likely the user deleted the results
                   database via some means external to LAMPWords, like the
                   Application Browser's "Delete" menu option. */
                /* TODO:  Should try better to ensure number is valid. */
                if (state.wordcount > (res - 1) * MAXLISTDBRECNODES &&
                    state.wordcount < res * MAXLISTDBRECNODES)
                {
                    /* Everything looks okay, open up the records. */
                    for (d = 0; d < res; d++)
                    {
                        listdbrecmem[d] = DmGetRecord(listdb, d);
                        if (listdbrecmem[d] != 0)
                        {
                            listdbrecptr[d] = MemHandleLock(listdbrecmem[d]);
                        }
                    }
                    /* Pretend we just finished a search.  This will nicely
                       update all of the relevant controls, including the
                       list. */
                    EndSearch();
                    /* Restore the previous position inside the list, if it
                       looks valid. */
                    if (state.topitem >= 0 && state.topitem < state.wordcount)
                    {
                        LstSetTopItem(wordlist, state.topitem);
                        LstDrawList(wordlist);
                        SclSetScrollBar(scrollbar,
                                        usegettop ? LstGetTopItem(wordlist) :
                                                    wordlist->topItem, 0,
                                        state.wordcount <= LISTSIZE ?
                                            0 : state.wordcount - LISTSIZE,
                                        LISTSIZE);
                    }
                }
                else
                {
                    state.wordcount = 0;
                }
            }
            else
            {
                state.wordcount = 0;
            }
        }

        handled = true;
        break;

    /* Close the form. */
    case frmCloseEvent:
        FrmEraseForm(curform);
        FrmDeleteForm(curform);
        curform = NULL;
        handled = true;
        break;

    /* Translate certain Graffiti(r) strokes and button presses. */
    case keyDownEvent:
        /* Capitalize letters. */
        if (event->data.keyDown.chr >= chrSmall_A &&
            event->data.keyDown.chr <= chrSmall_Z)
            event->data.keyDown.chr -= 32;
        /* alias for ' (vowel) (JLD) */
        if (event->data.keyDown.chr == chrApostrophe)
        {
            event->data.keyDown.chr = CHAR_VOWEL;
        }
        /* alias for - (consonant) (JLD) */
        if (event->data.keyDown.chr == chrHyphenMinus)
        {
            event->data.keyDown.chr = CHAR_CONSONANT;
        }

        /* Aliases for ? (blank). */
        if (event->data.keyDown.chr == chrFullStop ||  
            event->data.keyDown.chr == chrSpace)
            event->data.keyDown.chr = CHAR_BLANK;
        /* Return stroke search start shortcut. */
        if (event->data.keyDown.chr == chrLineFeed)
        {
            CtlHitControl(searchbut);
        }
        /* Up/down scroll buttons. */
        if (event->data.keyDown.chr == vchrPageUp)
        {
            LstScrollList(wordlist, winUp, LISTSIZE - 1);
            SclSetScrollBar(scrollbar,
                            usegettop ? LstGetTopItem(wordlist) :
                                        wordlist->topItem, 0,
                            state.wordcount <= LISTSIZE ?
                                0 : state.wordcount - LISTSIZE, LISTSIZE);
        }
        if (event->data.keyDown.chr == vchrPageDown)
        {
            LstScrollList(wordlist, winDown, LISTSIZE - 1);
            SclSetScrollBar(scrollbar,
                            usegettop ? LstGetTopItem(wordlist) :
                                        wordlist->topItem, 0,
                            state.wordcount <= LISTSIZE ?
                                0 : state.wordcount - LISTSIZE, LISTSIZE);
        }
        break;

    /* Handle buttons on the form. */
    case ctlSelectEvent:
        if (event->data.ctlSelect.controlID == QMButton)
        {
            EvtEnqueueKey('?', 0, 0);
            handled = true;
        }
        if (event->data.ctlSelect.controlID == StarButton)
        {
            EvtEnqueueKey('*', 0, 0);
            handled = true;
        }
        if (event->data.ctlSelect.controlID == VowelButton)
        {
            EvtEnqueueKey('^', 0, 0);
            handled = true;
        }
        if (event->data.ctlSelect.controlID == ConsButton)
        {
            EvtEnqueueKey('<', 0, 0);
            handled = true;
        }

        if (event->data.ctlSelect.controlID == SearchButton)
        {
            StartSearch(GetSearchMode(), false);
            handled = true;
        }
        if (event->data.ctlSelect.controlID == ClearButton)
        {
            SetFieldText(inputfield, "", MAXINPUTLENGTH + 1);
            handled = true;
        }

        if (event->data.ctlSelect.controlID == WJButton)
        {
            FrmPopupForm(WJForm);
            handled = true;
        }
        break;

    /* Scroll bar repeat. */
    case sclRepeatEvent:
        val = event->data.sclRepeat.newValue -
              event->data.sclRepeat.value;
        dir = winDown;
        if (val < 0)
        {
            dir = winUp;
            val = -val;
        }
        LstScrollList(wordlist, dir, val);
        break;

    /* Popup list selections. */
    case popSelectEvent:
        /* Fetch a string from the input history. */
        if (event->data.popSelect.controlP == histtrig)
        {
            ErrFatalDisplayIf(event->data.popSelect.selection < 0 ||
                              event->data.popSelect.selection >= HISTORYSIZE,
                              "Invalid selection from history list.");
            SetFieldText(inputfield,
                         state.inputhistory[event->data.popSelect.selection],
                         MAXINPUTLENGTH + 1);
            CtlSetLabel(histtrig, histstr);
            /* We always presume the user will want to see the top of the
               list by default - this will initialize it for next time. */
            LstSetSelection(histlist, 0);
            LstMakeItemVisible(histlist, 0);
            handled = true;
        }
        /* Change categories.  Note that the results aren't updated; the user
           will have to search again to update them. */
        if (event->data.popSelect.controlP == cattrig)
        {
            state.category = event->data.popSelect.selection;
        }
        break;

    /* Handle menu commands. */
    case menuEvent:
        switch (event->data.menu.itemID)
        {
            /* Help menu commands. */
            case HelpMenuInst:
                FrmHelp(InstStr);
                break;
            case HelpMenuDictInfo:
                /* CustomAlert boxes only support three strings and we've
                   got four so we have to glue them together. */
                StrCopy(dictinfo[0], dbhdrptr->title);
                StrCopy(dictinfo[1], dbhdrptr->desc);
                StrCopy(dictinfo[2], dbhdrptr->author);
                StrCat(dictinfo[2], "\n");
                StrCat(dictinfo[2], dbhdrptr->extra);
                FrmCustomAlert(DictInfoAlert,
                               dictinfo[0], dictinfo[1], dictinfo[2]);
                break;
            case HelpMenuAbout:
                FrmCustomAlert(AboutAlert, version, NULL, NULL);
                break;

            /* Standard edit menu functions. */
            /* Some documentation I've read implies that there's a way to
               have this setup and performed automatically, but I haven't
               figured out if this is true yet. */
            case sysEditMenuUndoCmd:
                FldUndo(inputfield);
                break;
            case sysEditMenuCutCmd:
                FldCut(inputfield);
                break;
            case sysEditMenuCopyCmd:
                FldCopy(inputfield);
                break;
            case sysEditMenuPasteCmd:
                FldPaste(inputfield);
                break;
            case sysEditMenuSelectAllCmd:
                FldSetSelection(inputfield, 0,
                                StrLen(FldGetTextPtr(inputfield)));
                break;
            case sysEditMenuKeyboardCmd:
                SysKeyboardDialog(kbdAlpha);
                break;
            case sysEditMenuGraffitiCmd:
                SysGraffitiReferenceDialog(referenceDefault);
                break;

            /* Options menu commands. */
            case OptMenuPrefs:
                FrmPopupForm(PrefsForm);
                break;
            case OptMenuClear:
                MemSet(state.inputhistory,
                       HISTORYSIZE * (MAXINPUTLENGTH + 1), 0);
                break;
            case OptMenuBeamDict:
                SendMe();
                break;
        }

        handled = true;
        break;

    /* Pendown inside list. */
    case lstEnterEvent:
        /* This is a bit of a kludge that updates the scroll bar whenever
           the pen is pressed inside the list.  This way we can respond to
           drag scrolling or presses of the little arrow buttons inside the
           list. */
        if (event->data.lstEnter.pList == wordlist)
        {
            LstHandleEvent(wordlist, event);
            SclSetScrollBar(scrollbar,
                            usegettop ? LstGetTopItem(wordlist) :
                                        wordlist->topItem, 0,
                            state.wordcount <= LISTSIZE ?
                                0 : state.wordcount - LISTSIZE, LISTSIZE);
            handled = true;
        }
        break;

    default:
        break;
    }

	return handled;
}

/*****************************************************************************/

/* PrefsFormEventHandler() - Handles events for the preferences dialog.
   Parameters:  event - Pointer to the event's data.
   Returns:  true if we handled the event, false if we didn't. */
Boolean PrefsFormEventHandler(EventPtr event)
{
    Boolean handled = false; /* Did we handle the event? */
    static FormPtr oldform;  /* Form that opened the preferences form. */

    switch (event->eType)
    {
    /* Open the form. */
    case frmOpenEvent:
        oldform = curform;
        curform = FrmGetActiveForm();
        /* Initialize controls. */
        LstSetSelection(maxreclist, prefs.listrecs - 1);
        LstMakeItemVisible(maxreclist, prefs.listrecs - 1);
        StrPrintF(maxrecbuf, "%i", prefs.listrecs);
        CtlSetLabel(maxrectrig, maxrecbuf);
        CtlSetValue(preslistchk, prefs.preservelist);
        CtlSetValue(showalphachk, prefs.showAlphagram);
        CtlSetValue(showsymchk, prefs.showSymbols);
        CtlSetValue(highlightmatchchk, prefs.highlightMatches);
        FrmDrawForm(curform);
        handled = true;
        break;

    /* Close the form. */
    case frmCloseEvent:
        FrmEraseForm(curform);
        FrmDeleteForm(curform);
        curform = oldform;
        handled = true;
        break;

    /* Handle buttons on the form. */
    case ctlSelectEvent:
        if (event->data.ctlSelect.controlID == PrefsOKButton)
        {
            /* Save the preferences.  Note they won't be stored in the
               database until the program ends. */
            prefs.listrecs = LstGetSelection(maxreclist) + 1;
            prefs.preservelist = CtlGetValue(preslistchk); 
            prefs.showAlphagram = CtlGetValue(showalphachk);
            prefs.showSymbols = CtlGetValue(showsymchk);
            prefs.highlightMatches = CtlGetValue(highlightmatchchk);
            curform = oldform;
            FrmReturnToForm(0);
            LstDrawList(wordlist);
            handled = true;
        }
        if (event->data.ctlSelect.controlID == PrefsCancelButton)
        {
            curform = oldform;
            FrmReturnToForm(0);
            handled = true;
        }
        break;

    default:
        break;
    }

    return handled;
}

/*****************************************************************************/

/* WJFormEventHandler() - Handles events for the word judge dialog.
   Parameters:  event - Pointer to the event's data.
   Returns:  true if we handled the event, false if we didn't. */
Boolean WJFormEventHandler(EventPtr event)
{
    Boolean handled = false; /* Did we handle the event? */
    static FormPtr oldform;  /* Form that opened the word judge form. */
    /* Used to concatenate the words for display in a CustomAlert. */
    static Char wordsbuf[(NUMWJWORDS * (MAXINPUTLENGTH + 1)) + 1];
    UInt8 d;                 /* Loop counter. */
    Boolean res;             /* Result code. */
    UInt16 id;               /* ID of control with current focus. */

    switch (event->eType)
    {
    /* Open the form. */
    case frmOpenEvent:
        oldform = curform;
        curform = FrmGetActiveForm();
        FrmDrawForm(curform);
        /* Initialize controls. */
        for (d = 0; d < NUMWJWORDS; d++)
        {
            SetFieldText(wjwordfield[d], "", MAXINPUTLENGTH + 1);
        }
        FrmSetFocus(curform, FrmGetObjectIndex(curform, WJField1));
        handled = true;
        break;

    /* Close the form. */
    case frmCloseEvent:
        FrmEraseForm(curform);
        FrmDeleteForm(curform);
        curform = oldform;
        handled = true;
        break;

    /* Handle buttons on the form. */
    case ctlSelectEvent:
        if (event->data.ctlSelect.controlID == WJJudgeButton)
        {
            /* Check if all fields are empty. */
            for (d = 0; d < NUMWJWORDS; d++)
            {
                if (FldGetTextLength(wjwordfield[d]) > 0)
                    break;
            }
            /* Search only if there's actually something written in one of
               the text fields. */
            if (d < NUMWJWORDS)
            {
                res = WordJudge();
                wordsbuf[0] = '\0';
                for (d = 0; d < NUMWJWORDS; d++)
                {
                    if (d > 0)
                    {
                        StrCat(wordsbuf, "\n");
                    }
                    StrCat(wordsbuf, wjwords[d]);
                }
                FrmCustomAlert(RulingAlert, wordsbuf,
                               res ? rulingtxt[1] : rulingtxt[0], NULL);
            }
            for (d = 0; d < NUMWJWORDS; d++)
            {
                SetFieldText(wjwordfield[d], "", MAXINPUTLENGTH + 1);
            }
            FrmSetFocus(curform, FrmGetObjectIndex(curform, WJField1));
            handled = true;
        }
        if (event->data.ctlSelect.controlID == WJClearButton)
        {
            for (d = 0; d < NUMWJWORDS; d++)
            {
                SetFieldText(wjwordfield[d], "", MAXINPUTLENGTH + 1);
            }
            FrmSetFocus(curform, FrmGetObjectIndex(curform, WJField1));
            handled = true;
        }
        if (event->data.ctlSelect.controlID == WJDoneButton)
        {
            curform = oldform;
            FrmReturnToForm(0);
            handled = true;
        }
        break;

    /* Translate certain Graffiti(r) strokes and button presses. */
    case keyDownEvent:
        /* Capitalize letters. */
        if (event->data.keyDown.chr >= chrSmall_A &&
            event->data.keyDown.chr <= chrSmall_Z)
            event->data.keyDown.chr -= 32;

        /* The return stroke is a shortcut key to move between fields. */
        if (event->data.keyDown.chr == chrLineFeed)
        {
            id = FrmGetObjectId(curform, FrmGetFocus(curform));
            /* Test if the current field is the bottommost one.  If so,
               go back to the top, otherwise move to the next field. */
            if (id == WJField1 + NUMWJWORDS - 1)
            {
                FrmSetFocus(curform,
                            FrmGetObjectIndex(curform, WJField1));
            }
            else
            {
                FrmSetFocus(curform,
                            FrmGetObjectIndex(curform, id + 1));
            }
        }
        break;

    default:
        break;
    }

    return handled;
}

/*****************************************************************************/

/* FitFormEventHandler() - Handles events for the fit pattern dialog.
   Parameters:  event - Pointer to the event's data.
   Returns:  true if we handled the event, false if we didn't. */
Boolean FitFormEventHandler(EventPtr event)
{
    Boolean handled = false; /* Did we handle the event? */
    static FormPtr oldform;  /* Form that opened the fit pattern form. */
    char buf[31];            /* Buffer for label text. */

    switch (event->eType)
    {
    /* Open the form. */
    case frmOpenEvent:
        oldform = curform;
        curform = FrmGetActiveForm();
        FrmDrawForm(curform);
        /* Initialize controls. */
        SetFieldText(fitfield, "", MAXINPUTLENGTH + 1);
        FrmSetFocus(curform, FrmGetObjectIndex(curform, FitField));
        StrPrintF(buf, "%s%s%s", fittext[0], origtext, fittext[1]);
        SetFieldText(fitlabel, buf, 31);
        handled = true;
        break;

    /* Close the form. */
    case frmCloseEvent:
        FrmEraseForm(curform);
        FrmDeleteForm(curform);
        curform = oldform;
        handled = true;
        break;

    /* Handle buttons on the form. */
    case ctlSelectEvent:
        if (event->data.ctlSelect.controlID == FitSearchButton)
        {
            textptr = FldGetTextPtr(fitfield);
            if (textptr != NULL)
            {
                StrCopy(searchpat, textptr);
                /* We want the fit pattern to be processed using Pattern mode
                   rules. */
                searchmode = modePattern;
                CleanUpStr(searchpat, false);
                searchmode = modeFit;
                searchpatlen = StrLen(searchpat);
                textptr = NULL;
            }
            curform = oldform;
            FrmReturnToForm(0);
            StartSearch(modeFit, true);
            handled = true;
        }
        if (event->data.ctlSelect.controlID == FitClearButton)
        {
            SetFieldText(fitfield, "", MAXINPUTLENGTH + 1);
            FrmSetFocus(curform, FrmGetObjectIndex(curform, FitField));
            handled = true;
        }
        if (event->data.ctlSelect.controlID == FitAbortButton)
        {
            curform = oldform;
            FrmReturnToForm(0);
            handled = true;
        }
        break;

    /* Translate certain Graffiti(r) strokes and button presses. */
    case keyDownEvent:
        /* Capitalize letters. */
        if (event->data.keyDown.chr >= chrSmall_A &&
            event->data.keyDown.chr <= chrSmall_Z)
            event->data.keyDown.chr -= 32;

        /* alias for ' (vowel) (JLD) */
        if (event->data.keyDown.chr == chrApostrophe)
        {
            event->data.keyDown.chr = CHAR_VOWEL;
        }
        /* alias for - (consonant) (JLD) */
        if (event->data.keyDown.chr == chrHyphenMinus)
        {
            event->data.keyDown.chr = CHAR_CONSONANT;
        }

        /* Aliases for ? (blank). */
        if (event->data.keyDown.chr == chrFullStop ||  
            event->data.keyDown.chr == chrSpace)
            event->data.keyDown.chr = CHAR_BLANK;

        /* The return stroke activates the Search button. */
        if (event->data.keyDown.chr == chrLineFeed)
        {
            CtlHitControl(fitsearchbut);
        }
        break;

    default:
        break;
    }

    return handled;
}

/*****************************************************************************/

/* AppEventHandler() - Handles events for whole program.
   Parameters:  event - Pointer to the event's data.
   Returns:  true if we handled the event, false if we didn't. */
static Boolean AppEventHandler(EventPtr event)
{
    Boolean handled = false; /* Did we handle the event? */
    FormPtr form;            /* Pointer used when loading a form. */
    UInt8 d;                 /* Loop counter. */

    switch (event->eType)
    {
    /* Load a form. */
    case frmLoadEvent:
        form = FrmInitForm(event->data.frmLoad.formID);
        FrmSetActiveForm(form);
        /* Since they're used fairly regularly I just pull a pointer
           to each of the controls (with a few exceptions) ahead of time so
           they're ready to use later. */
        if (event->data.frmLoad.formID == LWForm)
        {
            FrmSetEventHandler(form, LWFormEventHandler);
            wordlist = FrmGetObjectPtr(form,
                                   FrmGetObjectIndex(form, WordList));
            patpush = FrmGetObjectPtr(form,
                                   FrmGetObjectIndex(form, PatPush));
            anapush = FrmGetObjectPtr(form,
                                   FrmGetObjectIndex(form, AnaPush));
            bldpush = FrmGetObjectPtr(form,
                                   FrmGetObjectIndex(form, BldPush));
            fitpush = FrmGetObjectPtr(form,
                                   FrmGetObjectIndex(form, FitPush));
            inputfield = FrmGetObjectPtr(form,
                                   FrmGetObjectIndex(form, InputField));
            countfield = FrmGetObjectPtr(form,
                                   FrmGetObjectIndex(form, CountField));
            searchbut = FrmGetObjectPtr(form,
                                   FrmGetObjectIndex(form, SearchButton));
            scrollbar = FrmGetObjectPtr(form,
                                   FrmGetObjectIndex(form, WordScroll));
            histlist = FrmGetObjectPtr(form,
                                   FrmGetObjectIndex(form, HistList));
            histtrig = FrmGetObjectPtr(form,
                                   FrmGetObjectIndex(form, HistTrig));
            catlist = FrmGetObjectPtr(form,
                                   FrmGetObjectIndex(form, CatList));
            cattrig = FrmGetObjectPtr(form,
                                   FrmGetObjectIndex(form, CatTrig));
        }
        else if (event->data.frmLoad.formID == PrefsForm)
        {
            FrmSetEventHandler(form, PrefsFormEventHandler);
            maxreclist = FrmGetObjectPtr(form,
                             FrmGetObjectIndex(form, MaxRecList));
            maxrectrig = FrmGetObjectPtr(form,
                             FrmGetObjectIndex(form, MaxRecTrig));
            preslistchk = FrmGetObjectPtr(form,
                              FrmGetObjectIndex(form, PreserveListChk));
            showalphachk = FrmGetObjectPtr(form,
                              FrmGetObjectIndex(form, ShowAlphaChk));
            showsymchk = FrmGetObjectPtr(form,
                              FrmGetObjectIndex(form, ShowSymbolsChk));
            highlightmatchchk = FrmGetObjectPtr(form,
                                    FrmGetObjectIndex(form, 
                                                      HighlightWCMatchChk));
        }
        else if (event->data.frmLoad.formID == WJForm)
        {
            FrmSetEventHandler(form, WJFormEventHandler);
            for (d = 0; d < NUMWJWORDS; d++)
            {
                wjwordfield[d] = FrmGetObjectPtr(form,
                                     FrmGetObjectIndex(form, WJField1 + d));
            }
        }
        else if (event->data.frmLoad.formID == FitForm)
        {
            FrmSetEventHandler(form, FitFormEventHandler);
            fitlabel = FrmGetObjectPtr(form,
                                 FrmGetObjectIndex(form, FitLabel));
            fitfield = FrmGetObjectPtr(form,
                                 FrmGetObjectIndex(form, FitField));
            fitsearchbut = FrmGetObjectPtr(form,
                                   FrmGetObjectIndex(form, FitSearchButton));
        }
        handled = true;
        break;

    default:
        break;
	}

	return handled;
}

/*****************************************************************************/

/* EventLoop() - Fetches and dispatches events to the various handlers.
   Parameters:  None.
   Returns:  Nothing. */
static void EventLoop(void)
{
    EventType event;    /* Buffer that holds the event data. */
    UInt16 res;         /* Result code. */

    do
    {
        /* Grab an event.  If a search is in progress then we don't wait
           for one, otherwise we do. */
        EvtGetEvent(&event, searching ? 0 : evtWaitForever);

        /* If we're searching and nothing else happened we can continue
           the search. */
        if (event.eType == nilEvent && searching)
        {
            DoSearch();
        }

        /* Let the progress dialog handle events if we're using one, otherwise
           give it over to the system. */
        if (searching && useprog)
        {
            res = PrgHandleEvent(progbox, &event);
        }
        else
        {
            res = SysHandleEvent(&event);
        }
        
        /* Standard event-dispatching sequence. */
        if (!res)
        {
            if (!MenuHandleEvent(0, &event, &res))
            {
                if (!AppEventHandler(&event))
                {
					FrmDispatchEvent(&event);
				}
			}
		}
	}
	while (event.eType != appStopEvent);

}

/*****************************************************************************/
/* Program Starting Point.                                                   */
/*****************************************************************************/

/*****************************************************************************/

/* PilotMain() - Starting point for program execution.
   Parameters:  See Palm OS documentation.
   Returns:  See Palm OS documentation. */
UInt32 PilotMain(UInt16 launchCode, void* cmdPBP, UInt16 launchFlags)
{
    Err res = 0;

    /* We only handle a normal launch at this time.  There's not really a
       need to handle anything else. */
    if (launchCode == sysAppLaunchCmdNormalLaunch)
    {
        res = StartApplication();
        if (res == 0)
        {
			EventLoop();
			StopApplication();
		}
	}

    return res;
}

/* End of file:  lw.c */
