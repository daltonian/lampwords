LAMPWordsX - User Manual
========================

Table of Contents
-----------------

    Introduction
    Installation
    Tutorial - How to Use LAMPWords
    Search Modes
    Input History
    Word Judge Mode
    Dictionary Categories
    Menu Commands
    Changing Dictionaries
    Contacting the Author

Introduction
------------

    LAMPWords is a word finder and anagramming program for Palm OS handhelds.
Its name reflects this fact:  Lamp and Palm are anagrams.  LAMPWords is simple
to use and offers reasonably fast performance for common searches.

    LAMPWords is suitable for players of Scrabble(r), Boggle(r), Crossword
Puzzles, and other word games.  It can also be handy as a simple spelling
checker when writing.

    LAMPWords is free for all to use, and can be redistributed with little
restriction under the terms of the GNU General Public Licence (GPL).  See the
file "COPYING" for the full licence text.  The source code to LAMPWords is
available from the LAMPWords web site, in case anybody wants to modify the
program.

    This manual first covers installation and then offers a comprehensive
tutorial to teach you how to use it.  After that, the remaining sections are
mostly for reference.  You can read them straight through if you like but it's
not necessary to be able to use LAMPWords effectively.

Installation
------------

    LAMPWords installs like any other Palm OS program.  In case you have never
installed a Palm program, here is a short guide.  This guide applies to Windows
users only, but the steps on other operating systems should be similar.  For
more detailed information, consult your handheld's owner's manual.

    1) Download both LAMPWords and a dictionary database file from the
LAMPWords web site onto your PC.

    2) Create a new folder on your PC to hold LAMPWords.  It doesn't matter
where you put it, so choose a location that's convenient and easy to remember.

    3) Newer versions of Windows support Zip files from Windows Explorer.  In
this case, simply copy (unpack) the contents from the Zip folder to a
convenient location on your hard drive.  If Windows will not open the file in
Explorer, use a program like WinZip to unpack both LAMPWords and the dictionary
database.  Assuming you have WinZip installed you can just double-click on each
of the files you downloaded and WinZip should take you through the unpacking
process.  The only thing you'll need to do is tell WinZip where to put the
files when it asks what folder to unzip to - choose the folder you created in
step 2 for this purpose.  If you don't have WinZip, get it here:
http://www.winzip.com/

    4) In your Start Menu, under Programs, there should be a folder called
"Palm Desktop".  In this folder should be a program called Install Tool. Run
this program.  You should get a window that shows you what programs will be
installed on your Palm.  Click on Add, then find the folder where you unpacked
LAMPWords.  Inside this file should be a file called "LAMPWordsX" or
"LAMPWordsX.prc" - choose this and click OK.  The install tool should add it to
the list of programs to be installed.  Click on Add again and repeat, except
this time choose the dictionary database file.  The name depends on what
dictionary you downloaded before, but it will start with the letters "LWDB"
(e.g. "LWDB-TWL-1.pdb", "LWDB-SOWPODS-Compact-1.pdb", etc.)  Both files should
now be in the list.  Click Done to exit the install tool.

    5) Connect your Palm to its cradle or cable and do a HotSync.  Your Palm
should download LAMPWords and the dictionary from your PC.  While doing this
the Palm will say "Installing LAMPWords..." and "Installing LAMPWordsDB...".
It will take a little while to complete this step.

    6) That should do it!  You should see a LAMPWords icon amongst your other
Palm programs.

    7) If you need to save space on your computer, delete the folder you made
in step 2 and everything in it.  Otherwise, keep it so you can refer to the
information files stored inside it, or in case you need to reinstall LAMPWords.

    If you have problems with the installation, check the LAMPWords web site or
your handheld's documentation for tips.

Tutorial - How to Use LAMPWords
-------------------------------

    LAMPWords is very easy to use.  All of the major functionality of LAMPWords
is available from the main screen which is displayed when you start the
program, and this is where you will likely spend most of your time when using
LAMPWords.

    This tutorial will teach you how to use the major features of LAMPWords.
First you will learn how to look up just a single word.  Then you'll be shown
how to get its anagrams, then how to build a list of smaller words from that
word, followed by how to narrow down that list by fitting it to a pattern, and
finally how to use the wild card character to build even fancier lists.

    NOTE:  The tutorial assumes you've installed one of the Scrabble(r)
dictionary databases from the LAMPWords web site.  If you've installed another
dictionary you may see different results.

    NOTE:  Words/patterns to enter are shown below in quotation marks for
clarity.  Do not enter the question marks.  (Actually you can enter them if you
want, but LAMPWords will just discard them, so why waste your time?)

    1) For this example let's pick a simple, well-known word to look up.  I'll
use the word "STOP" because it's short, common, and has a few anagrams.

    2) Type or write the word "STOP" as you would enter text into any other
Palm application.  You can start writing or typing as soon as LAMPWords starts
running.  The recommended way to enter words is either by using Graffiti(r) or
an add-on keyboard, as LAMPWords will "clean up" your input as it goes.  For
example, LAMPWords uses all capital-letters, so if you enter a lower-case
letter via Graffiti it will be converted to a capital.  (This does not work for
international characters.)  If you use the on-screen keyboard or have a brand
new Palm device, the input will not be fixed up until you start the search.
This tutorial assumes you use Graffiti(r) or an add-on keyboard.  Note that if
you make a big mistake and need to start over, tap the Clear button on the
bottom to erase everything you just typed or wrote.

    3) Tap the PAT button on the left side of the screen, near the bottom - it
should darken.  (It might already be dark, but press it anyhow just to get the
hang of it.)  This tells LAMPWords you want to use Pattern Mode, which matches
the input in the order given.  For more information on Pattern Mode, see the
section below entitled "Search Modes".

    4) Press the Search button in the lower-left corner.  (Alternatively, you
can also use the Return stroke or press Enter or Return on your add-on keyboard
- this can be a handy timesaver.)  Your word should show up in the word list
(the giant box in the middle of the screen).  That's how easy it is!

    5) Now it's time to find some anagrams.  You probably know what anagrams
are already, but if not they're simply words with the same letters arranged in
a different order.  "STOP" has five anagrams (plus the word itself), all common
words.  See if you can find them all before you have LAMPWords generate a list!

    7) You may have noticed that "STOP" is still visible in the input area (the
underlined area near the bottom).  It's also darkened (highlighted), but we
won't worry about this just yet.  This means all you have to do to get the
anagrams is change modes and start the search again!

    8) Tap the ANA button, next to the PAT button.  It should darken and the
PAT button should lighten.  ANA activates Anagramming Mode, which finds the
anagrams of a word or jumble of letters.  Again, there's more information on it
later in this manual.

    9) Tap the Search button again, and LAMPWords will tell you the anagrams of
the word.  With "STOP", this will happen almost instantly.  You might see a box
flash up for a split second.  Don't worry about this right now, we'll get to it
later.

    10) Just for completeness, press the BLD button next to ANA and then press
Search again.  This time you'll get a list of all of the words than can be made
with the input letters, even if they're shorter.  This is called Build Mode
and, like the others, it's also described below.

    11) Now that you know how to use all three of LAMPWords' search modes, the
next step is to learn how to use the wild card (or "blank") character.  As its
name implies, the wild card character matches any letter.  Needless to say, the
wild card expands the possibility for word list generation substantially. We'll
first use the wild card to see what letters can be placed at the end of the
word you typed.  In Scrabble(r) parlance, these would be called the word's
"hooks".  For "STOP", you know an S after it will make "STOPS", but can you
think of any others?  Depending on the dictionary database you're using, there
will be one or two.

    12) "STOP" will still be highlighted in the input area.  This brings us to
an IMPORTANT point:  When the word (or a portion thereof) is highlighted, if
you type or write another letter the highlighted portion will be ERASED!  Most
of the time this will be what you want because it will let you rapidly make
repeated searches using different patterns, but in this case it's a bit of a
problem.  To avoid erasing your word, tap the underlined area to the right of
the word.  The word should un-highlight and you should see the cursor flashing
to the right of the word.  Of course you could also just reenter the word, but
for more complex searches or longer words that can be very inconvenient!  You
could also get the word from the Input History, which is covered in a separate
section later in this manual.

    13) The wild card (blank) character is "?", the question mark.  Type or
write the question mark once.  Your input area should now say "STOP?".  If
you're using Graffiti(r) you probably know that ? isn't the most convenient
character to write, so LAMPWords provides a few alternatives.  The first is the
convenience button on the right side, across from the mode buttons.  There's a
button marked ?, and when you tap it a ? will be placed in the Input area.  You
can also use a couple of alternative Graffiti(r) strokes:  period or space.  I
find period the easiest because I can give the writing area a quick double-tap,
but others may find the space stroke works better.  (? is used as the wild card
because it's the most common notation amongst Scrabble(r) players.)

    14) Tap PAT, then Search.  You'll see a list of words that start with
"STOP" and have a fifth letter at the end.  (Did you know the word "STOPE"?
It's a technique for excavation.)

    15) Now it's time to make things more interesting.  Press ANA and then
Search, and you'll get a list of anagrams that can be formed using the letters
in "STOP" plus one other letter.  You'll no doubt recognize some of them, while
others might simply look crazy!

    16) Even with my simple example of "STOP?", this list will be quite a bit
longer than any we've encountered so far.  In fact it will be too long to fit
entirely on screen.  LAMPWords provides several ways to "scroll" the list, and
you've probably already encountered them in other Palm applications.  The first
is the scroll bar on the right side of the screen.  (Apologies in advance to
left-handed users!)  Tapping it in various places will move you about the list.
The second method is by using the little arrows inside the box, at the corners.
Tapping these arrows moves the list up or down a "page".  Finally, if your
handheld has up and down arrow buttons on its case (most do), you can use these
to scroll up or down a page at a time.

    17) Press BLD and then Search.  This time the list will be very long!
Mostly that's due to the presence of the wild card, so use wild cards in Build
Mode with care.  On the other hand, you now know how to make LAMPWords generate
lists of hundreds of words, and it wasn't very hard at all now, was it?

    18) It's often handy to combine a Build or Anagram search with a Pattern
search to look up words matching a certain pattern that can be formed by a pool
of tiles.  (This is very handy in Scrabble(r) when you need to play a word
through one or more letters on the board.)  Fit Mode is the answer!  Fit Mode
essentially runs a Build Mode search followed by a Pattern Mode search.  To use
Fit Mode, tap the FIT button, next to the BLD button.

    19) We'll use the previous pattern of "STOP?" which should be highlighted
in the Input area, so tap Search again.  You'll see the Fit Pattern box.  This
lets you specify a pattern for the matching words.  Write or type "?O??".  This
will find all four-letter words with an O as the second letter that can be made
from the letters in "STOP" plus an additional letter.  Tap the Search button
inside the box to see the results.  There's still lots of words, but not as
many as before!

    20) Now let's do a search using different letters.  Type or write
"SATINE?".  As you enter the first character, the old word should disappear
because it was highlighted - see step 12.  Remember, you can use a period or
space in place of the question mark - see step 13.

    21) In Scrabble(r), "SATINE?" (where the ? represents a blank tile) is one
of the most ideal racks a player can get.  Just about every tournament players
will be able to find at least one word using all 7 of those tiles and earn the
50-point bonus, and I'll bet that a lot of casual players can find at least one
of them as well - can you?  Give it a try before moving on.

    22) Tap ANA and then tap Search.  Unless you've got a really new Palm
device that has a fast processor, this search will take a few seconds.  While
LAMPWords is searching you'll see a message box at the bottom of the screen,
telling you to wait.  This box has a couple of important features.  The first
is that it tells you what LAMPWords is actually trying to match.  If you enter
some garbage (e.g. punctuation) by mistake or use the on-screen keyboard,
LAMPWords will filter it out and try to conduct a meaningful search with what's
left, and it will show you the filtered input in this box.  The second
important feature is the Cancel button.  Complex searches might take several
minutes, and sometimes you might need to abort one (e.g. if you picked the wrong
mode before searching).  Tapping Cancel stops the search dead, and LAMPWords
will show you any words that were found before you aborted.  NOTE:  Older
handhelds will not show the box!  To abort a search on these devices, exit
LAMPWords by switching to another program and then restart LAMPWords.

    23) When the search is done, the box will disappear and the results will
be shown.  By the way, if you have any interest in competitive Scrabble(r) you
should learn all of the words from our "SATINE?" example by heart!  They show
up very frequently during games, and it's important to be able to find them to
cash in on that 50-point bonus.

    24) We'll do one more example.  Enter "??" in the input area.  (When you
enter the first ? the old input should disappear as noted earlier.  If not, tap
the Clear button to start fresh.)

    25) Tap PAT, then Search.  After a slight pause, you will be shown a list
of all of the two letter words in the dictionary.  I'll bet you didn't think
there were so many of them!

    26) Note that you could also perform the same search using Anagramming
Mode.  Try it:  Tap ANA, then Search.  You'll get the exact same list as in the
previous step.  However, Pattern Mode is the fastest mode, so use it whenever
possible.  Anagramming Mode is slower, and Build Mode is the slowest of all.

    This concludes the tutorial.  You now know enough about LAMPWords to use it
in your day-to-day life.  However, this tutorial didn't cover everything that
LAMPWords can do.  Read on if you wish to learn more about LAMPWords.  You
might also want to read on to see the answers to the questions below.  The name
of the section each question is answered in appears in brackets.

    - What does "Input History" mean?  [Input History]
    - What is that WJ button in the lower-right corner for?  [Word Judge Mode]
    - What does the list in the upper-right corner do?  [Dictionary Categories]
    - Why do some words have symbols after them?  [Dictionary Categories]
    - What menu options are available while using LAMPWords?  [Menu Commands]
    - What do all those small buttons on the right do?  [Special Characters]

Search Modes
------------

    LAMPWords has three search modes for generating words.  (There is a fourth
mode called Word Judge Mode which is somewhat different, hence it is covered in
a separate section below.)  Each search mode has several different uses which
will be described below, along with a full description of what each mode does.

    PATTERN MODE (PAT)

    Pattern Mode is accessed by tapping the PAT button.  When in Pattern Mode,
LAMPWords will match the input in the order you enter it.  LAMPWords will not
try to make anagrams or smaller words, it will just try to find words that fit
the pattern you describe.

    In Pattern Mode, the wild card character matches any letter in that
position.  Thus, to get a list of all seven-letter words ending in -ING, you
can enter:  "????ING".  There are also other special characters which can be
used to match more than one letter.  See the section on Special Characters
below.

    You can also use the * symbol in Pattern Mode.  The * symbol matches 0 or
more characters in a certain position.  This is very useful to find _ALL_ words
starting or ending with certain letters (not just those of a certain length).
For example, to find all the words beginning with ANTI-, enter "ANTI*".  To
find all the words ending in "ACTION", enter "*ACTION".  You can also use the *
in the middle of a pattern, or use more than one * characters in the same
pattern.  WARNING:  Searches that use the * character can be VERY SLOW!!!  The
closer to the beginning of the pattern a * is located, the longer the search
will take.  Multiple * characters will slow things down even more.

    Pattern Mode is the simplest, and for simple searches it's also the
fastest.  When you can perform the same search using multiple modes (e.g. the
search at the end of the tutorial), choose Pattern Mode to save a bit of time.

    Here are some suggested uses for Pattern Mode:

    - To look up a single word.  Simply type/write the entire word and tap
Search.  LAMPWords will show the word if it's in the dictionary, otherwise the
list will be empty.  NOTE:  For adjudicating Scrabble(r) plays, a separate Word
Judge mode is provided which can look up multiple words at once.

    - To see a word's "hooks".  A hook is a letter that can be placed before or
after a word to form another word.  For example, many words can be pluralized
by adding an S to the end of them, and some words can be negated by putting the
letter A in front.  To look up a word's hooks, enter the word with a question
mark in front or behind it.

    - To see a word's "extensions".  Extensions are just like hooks except with
multiple-letters, and in fact they are also called hooks by some players.  To
see extensions, enter a suitable number of wild cards (question marks) in front
of or behind the word.  The -ING example above will find the four-letter front
extensions of -ING.  (Of course, "ING" isn't a word, but you can still find its
extensions.)

    - To see words that have certain letters in certain spaces.  This is very
handy when doing Crossword Puzzles!  To do this, enter the pattern using a
question mark as an empty space and write the letters of filled-in intersecting
clues as needed.  For example, if you need an 8-letter word with the letter P
in the second position, a T in the fifth position, and an E in the seventh,
enter "?P??T?E?"

    - To see all the words of a certain length.  Simple enter the apporpriate
number of question marks.  Remember, you can use a period or space to represent
a question mark as they are somewhat easier to write with Graffiti(r).

    - To see all words ending or starting with certain letters, as discussed
above.

    ANAGRAMMING MODE (ANA)

    Anaramming Mode finds all words that can be made by arranging the letters
in the input in any order.  All letters and wild cards in the input must be
used, so the words in the list will always be of the same length as the input.
Anagramming Mode is accessed by tapping the ANA button.

    In Anagramming Mode, the ? symbol matches any letter in any position, but
only once.  So entering "LEAP?" will yield PANEL and APPLE (among others), but
not PANES because the wild card can only be used once.  Other special
characters (see below) work similarly.

    The * character works in anagramming mode in a very special way:  It will
tell LAMPWords to fine _ANY_ word containing the other letters in the input!
For example, to find words that contain the letter Z at least three times,
enter "*ZZZ".  Note that it doesn't matter where you put the *, and you only
need one of them.  WARNING:  ANAGRAM SEARCHES THAT USE * ARE _EXTREMELY_ SLOW!
Also, USE THIS WITH CARE - it's easy for lists to get too big if you're not
careful.  For example, "*" by itself will match EVERY WORD in the dictionary
until the results list fills up!

    Anagramming Mode is useful for:

    - Finding the anagrams of a word.  See the "STOP" example in the tutorial.

    - Finding the words one can make from the tiles on a Scrabble(r) rack.  See
the "SATINE?" example at the end of the tutorial.

    - Finding words of a certain length that contain certain letters.  For
example, to find all 7-letter words containing both a Q and a Z, enter
"QZ?????" into the input area.

    - Finding words of any length that contain certain characters, as discussed
above.

    BUILD MODE (BLD)

    Build Mode, accessed by tapping BLD, is essentially the same as Anagramming
Mode except that not all of the letters need be used.  Thus matching words will
be of any length up to and including the length of the input.

    As with Anagramming Mode, the wild card matches any letter in any position
once in Build Mode.

    It's important to note that if you use two wild cards in the input you will
match EVERY two-letter word in the database, plus any other words that match.
If you use three wild cards, you'll get all two-letter words PLUS all of the
three-letter words!  (Again, other special characters work similarly.)  As you
can see, the size of the list will swell up quite rapidly, so don't use more
wild cards than you need.  If you only need words in a limited range (e.g. 7 to
10 letters) consider using multiple Anagramming Mode searches instead.  If you
need words to fit to a particular pattern, look at Fit Mode (described next).

    NOTE:  The * character does not work in Build Mode.  It will be ignored if
entered.

    Build Mode has the following uses:

    - Finding all words that can be made from a big word.  Many of us will
remember such activities from grade school.  For example, to find all of the
words you can make from the letters in LAMPWords, enter "LAMPWORDS" into the
input area.

    - Finding all words that can be made from the tiles on a Scrabble(r) rack.

    - Finding words that can be made during a game of Boggle(r).  Note that
LAMPWords only supports words up to 15 letters long so you'll have to use a
subset of the letters shown on the dice.  Also note that LAMPWords doesn't
consider order, so you may see words that can't be formed given the arrangement
of the dice.

    FIT MODE (FIT)

    Fit Mode is accessed by tapping the FIT button.  Fit Mode is essentially a
combination of Build (or Anagram) and Pattern modes.

    To use Fit Mode, enter the "pool" of possible letters into the input area
as you would for a build.  Then tap Search, and the Fit Pattern window will pop
up.  In the Fit Pattern window, enter the desired pattern you wish the words to
conform to.  You can use any of the special characters in the pattern.  Tap
Search again to activate the search.

    Note that Fit Mode always uses a Build search before narrowing the words
down.  If you just want to find the anagrams of a certain word which fit a
certain pattern, simply make sure the length of the pattern is the same as the
length of the "pool" of letters.

    Fit Mode is useful for the following:

    - Finding words from letters on a Scrabble(r) rack that need to fit a
certain pattern in order to play through letters already on the board.

    - Finding words conforming to a pattern using a fixed pool of letters,
which is handy for certain types of word games.

Special Characters
------------------

    When searching, you can use several characters which have special meaning
to LAMPWords.  These characters, their use, and any alternative Graffiti(r)
strokes which might be easier to write are all listed below.

    In addition to the Graffiti strokes, there are Convenience Buttons on the
right side of the LAMPWords main screen which can be tapped to insert special
characters (except the digits) into the input area.

     Symbol   Meaning                                     Alternate Strokes
    -----------------------------------------------------------------------
       ?      Match any character exactly once.           . or space
       <      Match exactly one consonant.                -
       ^      Match exactly one vowel.                    '
      0-9     Match exactly one Scrabble(r) tile of the
    (digits)  digit's point value (0=ten points).
       *      Match 0 or more of any characters.          /

    NOTE:  Consonants, vowels, and Scrabble(r) tile point values are fixed for
English in the program and cannot be changed for international users.

Input History
-------------

    It's often the case that you need to conduct a bunch of similar searches in
a short period of time.  You might be talking with somebody about some
interesting words you found and they ask you to see the whole list.  Or maybe
you're studying and are trying to memorize some different lists, and you want
to periodically doublecheck words you can't quite remember.  Or maybe you did a
search recently and you just plain darn forgot some of the words it found!

    To help you out, LAMPWords provides the Input History.  The Input History
is simply a list of the last hundred input patterns that were used to perform
searches.

    The Input History is a popup list.  It's clearly labelled and is located
just below the search results box on the right side of the screen.  Tap the
words "Input History" to open the list.  The list contains the last hundred
distinct search patterns in order of use, with the most-recently used at the
top.  Select a pattern by tapping it.  Scroll the list by tapping the arrow
buttons on screen, or using the up/down buttons on your handheld.  If you
change your mind or don't see what you want, tap outside the list to get rid of
it.

    Note that only those input patterns that were actually used for searches
are saved in the Input History.  If you enter a pattern and clear it before
searching, it won't be saved.  Also, the input history won't duplicate any
patterns already in it.  If you repeat a pattern, it will simply be moved to
the top of the list.

    You can clear the Input History by opening the Options Menu and selecting
"Clear History".

Word Judge Mode
---------------

    A Palm device with LAMPWords can be used to adjudicate plays in
Scrabble(r).  To facilitate this, LAMPWords contains a special mode of
operation called Word Judge Mode.

    To invoke Word Judge Mode, tap the WJ button in the lower-right corner of
the screen, right below the results list box.  This will open the Word Judge
box.  While this box is open, only the features described in this section are
available.

    The Word Judge Box has five input areas into which you can enter the words
to be judged.  Enter one word per line.  Word Judge Mode does not allow wild
card or special characters - if you enter one it will be removed before the
search is performed.

    To help speed multiple-word adjudications, you can use a Graffiti(r) Return
stroke to move to the next input area.  Thus you can stay in the
Graffiti(r)-writing area while writing the whole play.  However, if you need to
go back and edit a word, tap on its input area (the underlined area) and edit
the text as you would in any other Palm program.  You can as few as one word or
as many as five.  Blank input fields will be ignored.

    Once the play has been entered, tap the Judge Play button to see the
result.  A box will pop up with the words on the top, and the ruling on the
bottom.  The words are displayed in the Ruling box so you can verify the words
judged were actually the ones you intended.  The ruling is ACCEPTABLE if all
words are found in the database.  It is NOT acceptable if at least one word is
not found.  Note that you won't be told which words weren't found, which is
what North American tournament Scrabble(r) rules require.

    NOTE:  The current Dictionary Category applies to Word Judge Mode.  Be sure
to select the correct category before tapping the WJ button.  In Word Judge
Mode the category will remain visible at the top of the screen so you can be
sure of what category is being used to judge the words.  See the section on
"Dictionary Categories" below for more information on categories.

    Tap OK to close the Ruling box.  The input areas will all be cleared and
you can judge another play.  The Clear button on the Word Judge box will also
clear all of the input areas for you.

    When you're done word judging, tap Done to go back to the LAMPWords main
screen.

Dictionary Categories
---------------------

    It can be useful to have multiple lists of words available to be searched.
Since LAMPWords only supports one word database, the words have to be divided
into categories.

    The category selector is in the upper-right corner of the screen.  The
current category will be displayed.  To select another category, tap the
currently selected category and a list of categories will appear.  Tap a
category name to select it, or tap outside the box to abort the change.

    When you select a category, it applies to future searches.  LAMPWords will
not update the current search results, so you must search again to see how the
category change affects the word list produced.

    Note that as mentioned above, the current category also applies to Word
Judge Mode.

    Although you may only search from one category, the dictionary database
author may have decided to provide a category that merges words from more than
one category.  In this case it might be useful to be able to tell which words
belong to which category, so the dictionary author may instruct LAMPWords to
display a symbol beside words belonging to specific categories.  These symbols
appear only in the search results box.  Consult any documentation that comes
with the dictionary database for specific details.  Symbols may be turned off
in the Preferences box (see below).

    The classic example of categories is to provide the North American (TWL),
British (OSW), and combined (SOWPODS) word lists for Scrabble(r).  In the
dictionaries provided on the LAMPWords web site, words in just the TWL have a
dollar sign ($) beside them, and words in just the OSW have a hash sign (#).
Words in both lists have no symbol.

Menu Commands
-------------

    LAMPWords is a fairly simple program so it only has a few menu commands.
To access the menus, tap the Menu button on your handheld, or tap the dark
LAMPWords title in the top left corner.

    Here is a list of menu commands:

    HELP

    Instructions - Provides a brief reminder of how to use LAMPWords, in case
you forget and don't have access to this manual.

    Dictionary Info - Displays information about the dictionary database
currently installed.  This information is written by the dictionary author and
is mainly to remind you what dictionary is installed.

    About LAMPWords - Displays the version of LAMPWords and author contact
information.

    EDIT

    The Edit menu provides text manipulation functions, e.g. Cut/Copy/Paste.
This is a standard menu on many Palm applications and as such it won't be
explained here.

    OPTIONS

    Preferences - Opens the Preferences box where you can alter LAMPWords'
behaviour slightly.  Inside the Preferences box are the following options:

        Max. List Records - Determines the maximum number of records that will
                            used to stored the search results list.  Each
                            record holds 2000 words, so the maximum number of
                            words that can be stored is 32000.  Each record of
                            2000 words takes up approximately 32K of your
                            Palm's memory, so if you are limited in memory you
                            might want to keep this setting low.  When the
                            search results list fills up LAMPWords will simply
                            stop the search and show what it found.

        Preserve List     - If this box is checked, LAMPWords will preserve the
                            contents of the search results list and restore it
                            next time you start LAMPWords.  The search results
                            list takes about 32K for every 2000 words, so if
                            you are low on memory then you may want to turn
                            this open.  However, most actions on a Palm don't
                            consume too much memory, so this setting can
                            normally be turned on.

        Show Alphagram    - When checked, LAMPWords will display the
                            alphabetical arrangement of the letters in each
                            word (called an "alphagram", for alphabetical
                            anagram) along with the word itself.  This can be a
                            very useful study aid.  Note that alphagrams are
                            only shown for words of up to ten letters in
                            lengths, due to screen space limitations on most
                            handhelds.  Changes to this setting take effect
                            after returning from the Preferences box.
                            
        Show Symbols	  - If checked, LAMPWords will show the symbols for
        					words as defined in the dictionary.  If off,
        					no symbols will be displayed.  When this setting is
        					changed it will take effect immediately upon
        					returning from the Preferences box.
        					
		Highlight ?* Matches  -	When this is turned on, LAMPWords will display
								any characters that matched either a ? or a *
								symbol in lower case to highlight them.
								Otherwise, words will be displayed in all-
								upper case.  Note that the regular font of
								most handhelds makes the upper-case I and the
								lower-case L look the same, and has a few other
								similarly-appearing characters.  Keep this in
								mind when looking up words with this setting
								turned on.  Changes to this setting will not
								take effect until the next 	search.

Changing Dictionaries
---------------------

    LAMPWords only supports one dictionary at a time.  As mentioned earlier,
the categories feature helps deal with this limitation somewhat.

    If you wish to change the dictionary, however, it is easy to do.
Replacement dictionaries are installed exactly like the dictionary you first
installed with LAMPWords.  Simply repeat the steps in the Installation section,
but omit the parts that install the LAMPWords program itself.

    Replacement dictionaries are available from the LAMPWords web site and may
also be made available by other users of LAMPWords.

    If you are interested in creating a dictionary for use with LAMPWords, see
the LAMPWords web site for information.

Contacting the Author
---------------------

	LAMPWords is no longer being supported by the author.  However, the web
site will be maintained for as long as possible and may be updated from time
to time:  http://members.shaw.ca/lampwords/

-- End of Manual --
